Pod::Spec.new do |s|
  s.name             = 'MKUIKit'
  s.version          = '0.3.1'
  s.summary          = 'MKSuites UI Framework'
  s.description      = <<-DESC
Loader, Toast and Taptic Engiene currently!
                       DESC
  s.homepage         = 'https://bitbucket.org/lachlangrant/MKUIKitPod'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'lachlangrant' => 'lachlangrant@rbvea.co' }
  s.source           = { :git => 'https://lachlangrant@bitbucket.org/lachlangrant/MKUIKit.git', :tag => s.version.to_s }
  s.ios.deployment_target = '11.0'
  s.source_files = 'MKUIKitPod/Classes/**/*'
  s.dependency 'SwiftyDrop', '~>4.0'
  s.frameworks = 'UIKit'
end
