# MKUIKitPod

[![CI Status](http://img.shields.io/travis/lachlangrant/MKUIKitPod.svg?style=flat)](https://travis-ci.org/lachlangrant/MKUIKitPod)
[![Version](https://img.shields.io/cocoapods/v/MKUIKitPod.svg?style=flat)](http://cocoapods.org/pods/MKUIKitPod)
[![License](https://img.shields.io/cocoapods/l/MKUIKitPod.svg?style=flat)](http://cocoapods.org/pods/MKUIKitPod)
[![Platform](https://img.shields.io/cocoapods/p/MKUIKitPod.svg?style=flat)](http://cocoapods.org/pods/MKUIKitPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MKUIKitPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MKUIKitPod'
```

## Author

lachlangrant, lachlangrant@rbvea.co

## License

MKUIKitPod is available under the MIT license. See the LICENSE file for more info.
