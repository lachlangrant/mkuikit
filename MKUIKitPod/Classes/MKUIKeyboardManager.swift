//
//  MKUIKeyboardManager.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import CoreGraphics
import UIKit

open class MKUIKeyboardManager: NSObject, UIGestureRecognizerDelegate {
	
	fileprivate static let  kMKUIDoneButtonToolbarTag = -1002
	fileprivate static let  kMKUIPreviousNextButtonToolbarTag = -1005
	fileprivate var registeredClasses  = [UIView.Type]()
	
	open var enable = false {
		
		didSet {
			if enable == true && oldValue == false {
				if _kbShowNotification != nil {
					keyboardWillShow(_kbShowNotification)
				}
				showLog("enabled")
			} else if enable == false &&
				oldValue == true {
				keyboardWillHide(nil)
				showLog("disabled")
			}
		}
	}
	
	fileprivate func privateIsEnabled()-> Bool {
		
		var isEnabled = enable
		
		if let textFieldViewController = _textFieldView?.inputViewController {
			
			if isEnabled == false {
				for enabledClass in enabledDistanceHandlingClasses {
					
					if textFieldViewController.isKind(of: enabledClass) {
						isEnabled = true
						break
					}
				}
			}
			
			if isEnabled == true {
				for disabledClass in disabledDistanceHandlingClasses {
					
					if textFieldViewController.isKind(of: disabledClass) {
						isEnabled = false
						break
					}
				}
			}
		}
		
		return isEnabled
	}
	
	
	open var keyboardDistanceFromTextField: CGFloat {
		set {
			_privateKeyboardDistanceFromTextField =  max(0, newValue)
			showLog("keyboardDistanceFromTextField: \(_privateKeyboardDistanceFromTextField)")
		}
		get {
			return _privateKeyboardDistanceFromTextField
		}
	}
	
	
	open var keyboardShowing: Bool {
		get {
			return _privateIsKeyboardShowing
		}
	}
	
	
	open var movedDistance: CGFloat {
		get {
			return _privateMovedDistance
		}
	}
	
	
	open var preventShowingBottomBlankSpace = true
	
	open class func sharedManager() -> MKUIKeyboardManager {
		
		struct Static {
			static let kbManager = MKUIKeyboardManager()
		}
		
		return Static.kbManager
	}
	
	open var enableAutoToolbar = true {
		didSet {
			
			privateIsEnableAutoToolbar() ?addToolbarIfRequired():removeToolbarIfRequired()
			
			let enableToolbar = enableAutoToolbar ? "Yes" : "NO"
			
			showLog("enableAutoToolbar: \(enableToolbar)")
		}
	}
	
	fileprivate func privateIsEnableAutoToolbar() -> Bool {
		
		var enableToolbar = enableAutoToolbar
		
		if let textFieldViewController = _textFieldView?.viewController() {
			
			if enableToolbar == false {
				
				//If found any toolbar enabled classes then return.
				for enabledClass in enabledToolbarClasses {
					
					if textFieldViewController.isKind(of: enabledClass) {
						enableToolbar = true
						break
					}
				}
			}
			
			if enableToolbar == true {
				for disabledClass in disabledToolbarClasses {
					
					if textFieldViewController.isKind(of: disabledClass) {
						enableToolbar = false
						break
					}
				}
			}
		}
		
		return enableToolbar
	}
	
	open var toolbarManageBehaviour = MKUIAutoToolbarManageBehaviour.bySubviews
	open var shouldToolbarUsesTextFieldTintColor = false
	open var toolbarTintColor : UIColor?

//	@available(*,deprecated, message: "Please use `previousNextDisplayMode` for better handling of previous/next button display. This property will be removed in future releases in favor of `previousNextDisplayMode`.")
//	open var shouldHidePreviousNext = false {
//		
//		didSet {
//			previousNextDisplayMode = shouldHidePreviousNext ? .alwaysHide : .Default
//		}
//	}
	open var previousNextDisplayMode = MKUIPreviousNextDisplayMode.Default
	open var toolbarDoneBarButtonItemImage : UIImage?
	open var toolbarDoneBarButtonItemText : String?
	open var shouldShowTextFieldPlaceholder = true
	open var placeholderFont: UIFont?

	fileprivate var         startingTextViewContentInsets = UIEdgeInsets.zero
	fileprivate var         startingTextViewScrollIndicatorInsets = UIEdgeInsets.zero
	fileprivate var         isTextViewContentInsetChanged = false
	
	
	open var overrideKeyboardAppearance = false
	
	
	open var keyboardAppearance = UIKeyboardAppearance.default

	
	open var shouldResignOnTouchOutside = false {
		
		didSet {
			_tapGesture.isEnabled = privateShouldResignOnTouchOutside()
			
			let shouldResign = shouldResignOnTouchOutside ? "Yes" : "NO"
			
			showLog("shouldResignOnTouchOutside: \(shouldResign)")
		}
	}
	
	fileprivate func privateShouldResignOnTouchOutside() -> Bool {
		
		var shouldResign = shouldResignOnTouchOutside
		
		if let textFieldViewController = _textFieldView?.inputViewController {
			
			if shouldResign == false {
				for enabledClass in enabledTouchResignedClasses {
					
					if textFieldViewController.isKind(of: enabledClass) {
						shouldResign = true
						break
					}
				}
			}
			
			if shouldResign == true {
				for disabledClass in disabledTouchResignedClasses {
					
					if textFieldViewController.isKind(of: disabledClass) {
						shouldResign = false
						break
					}
				}
			}
		}
		
		return shouldResign
	}
	
	
	@discardableResult open func resignFirstResponder()-> Bool {
		
		if let textFieldRetain = _textFieldView {
			let isResignFirstResponder = textFieldRetain.resignFirstResponder()
			
			if isResignFirstResponder == false {
				textFieldRetain.becomeFirstResponder()
				
				showLog("Refuses to resign first responder: \(String(describing: _textFieldView?._MKUIDescription()))")
			}
			
			return isResignFirstResponder
		}
		
		return false
	}
	
	
	open var canGoPrevious: Bool {
		if let textFields = responderViews() {
			if let  textFieldRetain = _textFieldView {
				
				if let index = textFields.index(of: textFieldRetain) {
					
					if index > 0 {
						return true
					}
				}
			}
		}
		return false
	}
	
	
	open var canGoNext: Bool {
		if let textFields = responderViews() {
			if let  textFieldRetain = _textFieldView {
				
				if let index = textFields.index(of: textFieldRetain) {
					
					if index < textFields.count-1 {
						return true
					}
				}
			}
		}
		return false
	}
	
	@discardableResult open func goPrevious()-> Bool {
		if let  textFieldRetain = _textFieldView {
			if let textFields = responderViews() {
				
				if let index = textFields.index(of: textFieldRetain) {
					
					if index > 0 {
						
						let nextTextField = textFields[index-1]
						
						let isAcceptAsFirstResponder = nextTextField.becomeFirstResponder()
						
						if isAcceptAsFirstResponder == false {
							textFieldRetain.becomeFirstResponder()
							
							showLog("Refuses to become first responder: \(nextTextField._MKUIDescription())")
						}
						
						return isAcceptAsFirstResponder
					}
				}
			}
		}
		
		return false
	}
	
	
	@discardableResult open func goNext()-> Bool {
		if let  textFieldRetain = _textFieldView {
			if let textFields = responderViews() {
				if let index = textFields.index(of: textFieldRetain) {
					if index < textFields.count-1 {
						
						let nextTextField = textFields[index+1]
						
						let isAcceptAsFirstResponder = nextTextField.becomeFirstResponder()
						
						if isAcceptAsFirstResponder == false {
							textFieldRetain.becomeFirstResponder()
							
							showLog("Refuses to become first responder: \(nextTextField._MKUIDescription())")
						}
						
						return isAcceptAsFirstResponder
					}
				}
			}
		}
		
		return false
	}
	
	
	@objc internal func previousAction (_ barButton : UIBarButtonItem?) {
		if shouldPlayInputClicks == true {
			UIDevice.current.playInputClick()
		}
		
		if canGoPrevious == true {
			
			if let textFieldRetain = _textFieldView {
				let isAcceptAsFirstResponder = goPrevious()
				
				if isAcceptAsFirstResponder &&
					textFieldRetain.previousInvocation.target != nil &&
					textFieldRetain.previousInvocation.action != nil {
					
					UIApplication.shared.sendAction(textFieldRetain.previousInvocation.action!, to: textFieldRetain.previousInvocation.target, from: textFieldRetain, for: UIEvent())
				}
			}
		}
	}
	
	
	@objc internal func nextAction (_ barButton : UIBarButtonItem?) {
		if shouldPlayInputClicks == true {
			UIDevice.current.playInputClick()
		}
		
		if canGoNext == true {
			
			if let textFieldRetain = _textFieldView {
				let isAcceptAsFirstResponder = goNext()
				
				if isAcceptAsFirstResponder &&
					textFieldRetain.nextInvocation.target != nil &&
					textFieldRetain.nextInvocation.action != nil {
					
					UIApplication.shared.sendAction(textFieldRetain.nextInvocation.action!, to: textFieldRetain.nextInvocation.target, from: textFieldRetain, for: UIEvent())
				}
			}
		}
	}
	
	
	@objc internal func doneAction (_ barButton : MKUIBarButtonItem?) {
		if shouldPlayInputClicks == true {
			UIDevice.current.playInputClick()
		}
		
		if let textFieldRetain = _textFieldView {
			let isResignedFirstResponder = resignFirstResponder()
			
			if isResignedFirstResponder &&
				textFieldRetain.doneInvocation.target != nil &&
				textFieldRetain.doneInvocation.action != nil{
				
				UIApplication.shared.sendAction(textFieldRetain.doneInvocation.action!, to: textFieldRetain.doneInvocation.target, from: textFieldRetain, for: UIEvent())
			}
		}
	}
	
	@objc internal func tapRecognized(_ gesture: UITapGestureRecognizer) {
		
		if gesture.state == UIGestureRecognizerState.ended {
			
			_ = resignFirstResponder()
		}
	}
	
	
	open func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return false
	}
	
	
	open func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
		return !(touch.view is UIControl ||
			touch.view is UINavigationBar)
	}
	
	open var shouldPlayInputClicks = true
	open var layoutIfNeededOnUpdate = false
	open var shouldFixInteractivePopGestureRecognizer = true
	open var disabledDistanceHandlingClasses  = [UIViewController.Type]()
	open var enabledDistanceHandlingClasses  = [UIViewController.Type]()
	open var disabledToolbarClasses  = [UIViewController.Type]()
	open var enabledToolbarClasses  = [UIViewController.Type]()
	open var toolbarPreviousNextAllowedClasses  = [UIView.Type]()
	open var disabledTouchResignedClasses  = [UIViewController.Type]()
	open var enabledTouchResignedClasses  = [UIViewController.Type]()
	
	open func registerTextFieldViewClass(_ aClass: UIView.Type, didBeginEditingNotificationName : String, didEndEditingNotificationName : String) {
		
		registeredClasses.append(aClass)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.textFieldViewDidBeginEditing(_:)),    name: NSNotification.Name(rawValue: didBeginEditingNotificationName), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.textFieldViewDidEndEditing(_:)),      name: NSNotification.Name(rawValue: didEndEditingNotificationName), object: nil)
	}
	
	fileprivate weak var    _textFieldView: UIView?
	fileprivate var         _topViewBeginRect = CGRect.zero
	fileprivate weak var    _rootViewController: UIViewController?
	fileprivate var         _layoutGuideConstraintInitialConstant: CGFloat  = 0
	fileprivate weak var    _layoutGuideConstraint: NSLayoutConstraint?
	fileprivate weak var    _lastScrollView: UIScrollView?
	fileprivate var         _startingContentOffset = CGPoint.zero
	fileprivate var         _startingScrollIndicatorInsets = UIEdgeInsets.zero
	fileprivate var         _startingContentInsets = UIEdgeInsets.zero
	fileprivate var         _kbShowNotification: Notification?
	fileprivate var         _kbSize = CGSize.zero
	fileprivate var         _statusBarFrame = CGRect.zero
	fileprivate var         _animationDuration = 0.25
	fileprivate var         _animationCurve = UIViewAnimationOptions.curveEaseOut
	fileprivate var         _tapGesture: UITapGestureRecognizer!
	fileprivate var         _privateIsKeyboardShowing = false
	fileprivate var         _privateMovedDistance : CGFloat = 0.0
	fileprivate var         _privateKeyboardDistanceFromTextField: CGFloat = 10.0
	

	override init() {
		super.init()
		NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
		
		registerTextFieldViewClass(UITextField.self, didBeginEditingNotificationName: NSNotification.Name.UITextFieldTextDidBeginEditing.rawValue, didEndEditingNotificationName: NSNotification.Name.UITextFieldTextDidEndEditing.rawValue)
		registerTextFieldViewClass(UITextView.self, didBeginEditingNotificationName: NSNotification.Name.UITextViewTextDidBeginEditing.rawValue, didEndEditingNotificationName: NSNotification.Name.UITextViewTextDidEndEditing.rawValue)
		NotificationCenter.default.addObserver(self, selector: #selector(self.willChangeStatusBarOrientation(_:)), name: NSNotification.Name.UIApplicationWillChangeStatusBarOrientation, object: UIApplication.shared)
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.didChangeStatusBarFrame(_:)),          name: NSNotification.Name.UIApplicationDidChangeStatusBarFrame, object: UIApplication.shared)
		
		_tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapRecognized(_:)))
		_tapGesture.cancelsTouchesInView = false
		_tapGesture.delegate = self
		_tapGesture.isEnabled = shouldResignOnTouchOutside
		
		let textField = UITextField()
		textField.addDoneOnKeyboardWithTarget(nil, action: #selector(self.doneAction(_:)))
		textField.addPreviousNextDoneOnKeyboardWithTarget(nil, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), doneAction: #selector(self.doneAction(_:)))
		
		disabledDistanceHandlingClasses.append(UITableViewController.self)
		disabledDistanceHandlingClasses.append(UIAlertController.self)
		disabledToolbarClasses.append(UIAlertController.self)
		disabledTouchResignedClasses.append(UIAlertController.self)
		toolbarPreviousNextAllowedClasses.append(UITableView.self)
		toolbarPreviousNextAllowedClasses.append(UICollectionView.self)
		toolbarPreviousNextAllowedClasses.append(MKUIPreviousNextView.self)
		
		struct InternalClass {
			
			static var UIAlertControllerTextFieldViewController: UIViewController.Type?  =   NSClassFromString("_UIAlertControllerTextFieldViewController") as? UIViewController.Type
		}
		
		if let aClass = InternalClass.UIAlertControllerTextFieldViewController {
			disabledDistanceHandlingClasses.append(aClass.self)
			disabledToolbarClasses.append(aClass.self)
			disabledTouchResignedClasses.append(aClass.self)
		}
	}
	
	
	
	//    override public class func load() {
	//        super.load()
	//
	//        //Enabling MKUIKeyboardManager.
	//        MKUIKeyboardManager.sharedManager().enable = true
	//    }
	
	deinit {
		enable = false
		NotificationCenter.default.removeObserver(self)
	}
	
	
	fileprivate func keyWindow() -> UIWindow? {
		
		if let keyWindow = _textFieldView?.window {
			return keyWindow
		} else {
			
			struct Static {
				static var keyWindow : UIWindow?
			}
			
			let originalKeyWindow = UIApplication.shared.keyWindow
			
			if originalKeyWindow != nil &&
				(Static.keyWindow == nil || Static.keyWindow != originalKeyWindow) {
				Static.keyWindow = originalKeyWindow
			}
			
			return Static.keyWindow
		}
	}
	
	fileprivate func setRootViewFrame(_ frame: CGRect) {
		
		var controller = _textFieldView?.topMostController()
		
		if controller == nil {
			controller = keyWindow()?.topMostController()
		}
		
		if let unwrappedController = controller {
			
			var newFrame = frame
			newFrame.size = unwrappedController.view.frame.size
			
			UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
				
				unwrappedController.view.frame = newFrame
				self.showLog("Set \(String(describing: controller?._MKUIDescription())) frame to : \(newFrame)")
				
				if self.layoutIfNeededOnUpdate == true {
					unwrappedController.view.setNeedsLayout()
					unwrappedController.view.layoutIfNeeded()
				}
				
				
			}) { (animated:Bool) -> Void in}
		} else {
			showLog("You must set UIWindow.rootViewController in your AppDelegate to work with MKUIKeyboardManager")
		}
	}
	
	fileprivate func adjustFrame() {
		if _textFieldView == nil {
			return
		}
		
		let textFieldView = _textFieldView!
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		let optionalWindow = keyWindow()
		
		var optionalRootController = _textFieldView?.topMostController()
		if optionalRootController == nil {
			optionalRootController = keyWindow()?.topMostController()
		}
		
		let optionalTextFieldViewRect = textFieldView.superview?.convert(textFieldView.frame, to: optionalWindow)
		
		if optionalRootController == nil ||
			optionalWindow == nil ||
			optionalTextFieldViewRect == nil {
			return
		}
		
		let rootController = optionalRootController!
		let window = optionalWindow!
		let textFieldViewRect = optionalTextFieldViewRect!
		
		var rootViewRect = rootController.view.frame
		var specialKeyboardDistanceFromTextField = textFieldView.keyboardDistanceFromTextField
		
		if textFieldView.isSearchBarTextField() {
			
			if  let searchBar = textFieldView.superviewOfClassType(UISearchBar.self) {
				specialKeyboardDistanceFromTextField = searchBar.keyboardDistanceFromTextField
			}
		}
		
		let newKeyboardDistanceFromTextField = (specialKeyboardDistanceFromTextField == kMKUIUseDefaultKeyboardDistance) ? keyboardDistanceFromTextField : specialKeyboardDistanceFromTextField
		var kbSize = _kbSize
		kbSize.height += newKeyboardDistanceFromTextField
		
		let statusBarFrame = UIApplication.shared.statusBarFrame
		
		var layoutGuidePosition = MKUILayoutGuidePosition.none
		
		if let viewController = textFieldView.viewController() {
			
			if let constraint = _layoutGuideConstraint {
				
				var layoutGuide : UILayoutSupport?
				if let itemLayoutGuide = constraint.firstItem as? UILayoutSupport {
					layoutGuide = itemLayoutGuide
				} else if let itemLayoutGuide = constraint.secondItem as? UILayoutSupport {
					layoutGuide = itemLayoutGuide
				}
				
				if let itemLayoutGuide : UILayoutSupport = layoutGuide {
					
					if (itemLayoutGuide === viewController.topLayoutGuide)    //If topLayoutGuide constraint
					{
						layoutGuidePosition = .top
					}
					else if (itemLayoutGuide === viewController.bottomLayoutGuide)    //If bottomLayoutGuice constraint
					{
						layoutGuidePosition = .bottom
					}
				}
			}
		}
		
		let topLayoutGuide : CGFloat = statusBarFrame.height
		
		var move : CGFloat = 0.0
		
		if layoutGuidePosition == .bottom {
			move = textFieldViewRect.maxY-(window.frame.height-kbSize.height)
		} else {
			move = min(textFieldViewRect.minY-(topLayoutGuide+5), textFieldViewRect.maxY-(window.frame.height-kbSize.height))
		}
		
		showLog("Need to move: \(move)")
		
		var superScrollView : UIScrollView? = nil
		var superView = textFieldView.superviewOfClassType(UIScrollView.self) as? UIScrollView
		
		while let view = superView {
			
			if (view.isScrollEnabled) {
				superScrollView = view
				break
			}
			else {
				superView = view.superviewOfClassType(UIScrollView.self) as? UIScrollView
			}
		}
		
		if let lastScrollView = _lastScrollView {
			if superScrollView == nil {
				
				showLog("Restoring \(lastScrollView._MKUIDescription()) contentInset to : \(_startingContentInsets) and contentOffset to : \(_startingContentOffset)")
				
				UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
					
					lastScrollView.contentInset = self._startingContentInsets
					lastScrollView.scrollIndicatorInsets = self._startingScrollIndicatorInsets
				}) { (animated:Bool) -> Void in }
				
				if lastScrollView.shouldRestoreScrollViewContentOffset == true {
					lastScrollView.setContentOffset(_startingContentOffset, animated: true)
				}
				
				_startingContentInsets = UIEdgeInsets.zero
				_startingScrollIndicatorInsets = UIEdgeInsets.zero
				_startingContentOffset = CGPoint.zero
				_lastScrollView = nil
			} else if superScrollView != lastScrollView {     //If both scrollView's are different, then reset lastScrollView to it's original frame and setting current scrollView as last scrollView.
				
				showLog("Restoring \(lastScrollView._MKUIDescription()) contentInset to : \(_startingContentInsets) and contentOffset to : \(_startingContentOffset)")
				
				UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
					
					lastScrollView.contentInset = self._startingContentInsets
					lastScrollView.scrollIndicatorInsets = self._startingScrollIndicatorInsets
				}) { (animated:Bool) -> Void in }
				
				if lastScrollView.shouldRestoreScrollViewContentOffset == true {
					lastScrollView.setContentOffset(_startingContentOffset, animated: true)
				}
				
				_lastScrollView = superScrollView
				_startingContentInsets = superScrollView!.contentInset
				_startingScrollIndicatorInsets = superScrollView!.scrollIndicatorInsets
				_startingContentOffset = superScrollView!.contentOffset
				
				showLog("Saving New \(lastScrollView._MKUIDescription()) contentInset : \(_startingContentInsets) and contentOffset : \(_startingContentOffset)")
			}
		} else if let unwrappedSuperScrollView = superScrollView {
			_lastScrollView = unwrappedSuperScrollView
			_startingContentInsets = unwrappedSuperScrollView.contentInset
			_startingScrollIndicatorInsets = unwrappedSuperScrollView.scrollIndicatorInsets
			_startingContentOffset = unwrappedSuperScrollView.contentOffset
			
			showLog("Saving \(unwrappedSuperScrollView._MKUIDescription()) contentInset : \(_startingContentInsets) and contentOffset : \(_startingContentOffset)")
		}
		
		if let lastScrollView = _lastScrollView {
			var lastView = textFieldView
			var superScrollView = _lastScrollView
			
			while let scrollView = superScrollView {
				if move > 0 ? (move > (-scrollView.contentOffset.y - scrollView.contentInset.top)) : scrollView.contentOffset.y>0 {
					
					var tempScrollView = scrollView.superviewOfClassType(UIScrollView.self) as? UIScrollView
					var nextScrollView : UIScrollView? = nil
					while let view = tempScrollView {
						
						if (view.isScrollEnabled) {
							nextScrollView = view
							break
						} else {
							tempScrollView = view.superviewOfClassType(UIScrollView.self) as? UIScrollView
						}
					}
					
					if let lastViewRect = lastView.superview?.convert(lastView.frame, to: scrollView) {
						var shouldOffsetY = scrollView.contentOffset.y - min(scrollView.contentOffset.y,-move)
						shouldOffsetY = min(shouldOffsetY, lastViewRect.origin.y)
						
						if textFieldView is UITextView == true &&
							nextScrollView == nil &&
							shouldOffsetY >= 0 {
							var maintainTopLayout : CGFloat = 0
							
							if let navigationBarFrame = textFieldView.viewController()?.navigationController?.navigationBar.frame {
								maintainTopLayout = navigationBarFrame.maxY
							}
							
							maintainTopLayout += 10.0
							
							if let currentTextFieldViewRect = textFieldView.superview?.convert(textFieldView.frame, to: window) {
								
								let expectedFixDistance = currentTextFieldViewRect.minY - maintainTopLayout
								
								shouldOffsetY = min(shouldOffsetY, scrollView.contentOffset.y + expectedFixDistance)
								
								move = 0
							}
							else {
								move -= (shouldOffsetY-scrollView.contentOffset.y)
							}
						}
						else
						{
							move -= (shouldOffsetY-scrollView.contentOffset.y)
						}
						
						UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
							
							self.showLog("Adjusting \(scrollView.contentOffset.y-shouldOffsetY) to \(scrollView._MKUIDescription()) ContentOffset")
							
							self.showLog("Remaining Move: \(move)")
							
							scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: shouldOffsetY)
						}) { (animated:Bool) -> Void in }
					}
					
					lastView = scrollView
					superScrollView = nextScrollView
				} else {
					break
				}
			}
			
			if let lastScrollViewRect = lastScrollView.superview?.convert(lastScrollView.frame, to: window) {
				
				let bottom : CGFloat = kbSize.height-newKeyboardDistanceFromTextField-(window.frame.height-lastScrollViewRect.maxY)
				
				var movedInsets = lastScrollView.contentInset
				
				movedInsets.bottom = max(_startingContentInsets.bottom, bottom)
				
				showLog("\(lastScrollView._MKUIDescription()) old ContentInset : \(lastScrollView.contentInset)")
				
				UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
					lastScrollView.contentInset = movedInsets
					
					var newInset = lastScrollView.scrollIndicatorInsets
					newInset.bottom = movedInsets.bottom
					lastScrollView.scrollIndicatorInsets = newInset
					
				}) { (animated:Bool) -> Void in }
				
				showLog("\(lastScrollView._MKUIDescription()) new ContentInset : \(lastScrollView.contentInset)")
			}
		}
		
		if layoutGuidePosition == .top {
			
			if let constraint = _layoutGuideConstraint {
				
				let constant = min(_layoutGuideConstraintInitialConstant, constraint.constant-move)
				
				UIView.animate(withDuration: _animationDuration, delay: 0, options: (_animationCurve.union(UIViewAnimationOptions.beginFromCurrentState)), animations: { () -> Void in
					
					constraint.constant = constant
					self._rootViewController?.view.setNeedsLayout()
					self._rootViewController?.view.layoutIfNeeded()
					
				}, completion: { (finished) -> Void in })
			}
			
		} else if layoutGuidePosition == .bottom {
			
			if let constraint = _layoutGuideConstraint {
				
				let constant = max(_layoutGuideConstraintInitialConstant, constraint.constant+move)
				
				UIView.animate(withDuration: _animationDuration, delay: 0, options: (_animationCurve.union(UIViewAnimationOptions.beginFromCurrentState)), animations: { () -> Void in
					
					constraint.constant = constant
					self._rootViewController?.view.setNeedsLayout()
					self._rootViewController?.view.layoutIfNeeded()
					
				}, completion: { (finished) -> Void in })
			}
			
		} else {
			if let textView = textFieldView as? UITextView {
				let textViewHeight = min(textView.frame.height, (window.frame.height-kbSize.height-(topLayoutGuide)))
				
				if (textView.frame.size.height-textView.contentInset.bottom>textViewHeight)
				{
					UIView.animate(withDuration: _animationDuration, delay: 0, options: (_animationCurve.union(UIViewAnimationOptions.beginFromCurrentState)), animations: { () -> Void in
						
						self.showLog("\(textFieldView._MKUIDescription()) Old UITextView.contentInset : \(textView.contentInset)")
						
						if (self.isTextViewContentInsetChanged == false)
						{
							self.startingTextViewContentInsets = textView.contentInset
							self.startingTextViewScrollIndicatorInsets = textView.scrollIndicatorInsets
						}
						
						var newContentInset = textView.contentInset
						newContentInset.bottom = textView.frame.size.height-textViewHeight
						textView.contentInset = newContentInset
						textView.scrollIndicatorInsets = newContentInset
						self.isTextViewContentInsetChanged = true
						
						self.showLog("\(textFieldView._MKUIDescription()) Old UITextView.contentInset : \(textView.contentInset)")
						
						
					}, completion: { (finished) -> Void in })
				}
			}
			
			if rootController.modalPresentationStyle == UIModalPresentationStyle.formSheet ||
				rootController.modalPresentationStyle == UIModalPresentationStyle.pageSheet {
				
				showLog("Found Special case for Model Presentation Style: \(rootController.modalPresentationStyle)")
				
				if move >= 0 {
					rootViewRect.origin.y -= move
					
					if preventShowingBottomBlankSpace == true {
						let minimumY: CGFloat = (window.frame.height-rootViewRect.size.height-topLayoutGuide)/2-(kbSize.height-newKeyboardDistanceFromTextField)
						
						rootViewRect.origin.y = max(rootViewRect.minY, minimumY)
					}
					
					showLog("Moving Upward")
					setRootViewFrame(rootViewRect)
					_privateMovedDistance = (_topViewBeginRect.origin.y-rootViewRect.origin.y)
				} else {
					let disturbDistance = rootViewRect.minY-_topViewBeginRect.minY
					
					if disturbDistance < 0 {
						rootViewRect.origin.y -= max(move, disturbDistance)
						
						showLog("Moving Downward")
						setRootViewFrame(rootViewRect)
						_privateMovedDistance = (_topViewBeginRect.origin.y-rootViewRect.origin.y)
					}
				}
			} else {
				if move >= 0 {
					
					rootViewRect.origin.y -= move
					
					if preventShowingBottomBlankSpace == true {
						
						rootViewRect.origin.y = max(rootViewRect.origin.y, min(0, -kbSize.height+newKeyboardDistanceFromTextField))
					}
					
					showLog("Moving Upward")
					setRootViewFrame(rootViewRect)
					_privateMovedDistance = (_topViewBeginRect.origin.y-rootViewRect.origin.y)
				} else {
					let disturbDistance : CGFloat = rootViewRect.minY-_topViewBeginRect.minY
					
					if disturbDistance < 0 {
						
						rootViewRect.origin.y -= max(move, disturbDistance)
						
						showLog("Moving Downward")
						setRootViewFrame(rootViewRect)
						_privateMovedDistance = (_topViewBeginRect.origin.y-rootViewRect.origin.y)
					}
				}
			}
		}
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	open func reloadLayoutIfNeeded() -> Void {
		
		if privateIsEnabled() == true {
			if _textFieldView != nil &&
				_privateIsKeyboardShowing == true &&
				_topViewBeginRect.equalTo(CGRect.zero) == false &&
				_textFieldView?.isAlertViewTextField() == false {
				adjustFrame()
			}
		}
	}
	
	@objc internal func keyboardWillShow(_ notification : Notification?) -> Void {
		
		_kbShowNotification = notification
		_privateIsKeyboardShowing = true
		
		if privateIsEnabled() == false {
			return
		}
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		if _textFieldView != nil && _topViewBeginRect.equalTo(CGRect.zero) == true {
			
			if let constraint = _textFieldView?.viewController()?.MKUILayoutGuideConstraint {
				_layoutGuideConstraint = constraint
				_layoutGuideConstraintInitialConstant = constraint.constant
			}
			
			_rootViewController = _textFieldView?.topMostController()
			if _rootViewController == nil {
				_rootViewController = keyWindow()?.topMostController()
			}
			
			if let unwrappedRootController = _rootViewController {
				_topViewBeginRect = unwrappedRootController.view.frame
				
				if shouldFixInteractivePopGestureRecognizer == true &&
					unwrappedRootController is UINavigationController &&
					unwrappedRootController.modalPresentationStyle != UIModalPresentationStyle.formSheet &&
					unwrappedRootController.modalPresentationStyle != UIModalPresentationStyle.pageSheet {
					
					if let window = keyWindow() {
						_topViewBeginRect.origin = CGPoint(x: 0,y: window.frame.size.height-unwrappedRootController.view.frame.size.height)
					} else {
						_topViewBeginRect.origin = CGPoint.zero
					}
				}
				
				showLog("Saving \(unwrappedRootController._MKUIDescription()) beginning Frame: \(_topViewBeginRect)")
			} else {
				_topViewBeginRect = CGRect.zero
			}
		}
		
		let oldKBSize = _kbSize
		
		if let info = (notification as NSNotification?)?.userInfo {
			if let curve = (info[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue {
				_animationCurve = UIViewAnimationOptions(rawValue: curve)
			} else {
				_animationCurve = UIViewAnimationOptions.curveEaseOut
			}

			if let duration = (info[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue {
				if duration != 0.0 {
					_animationDuration = duration
				}
			} else {
				_animationDuration = 0.25
			}
			
			if let kbFrame = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
				
				let screenSize = (UIApplication.shared.keyWindow?.bounds)!
				
				let intersectRect = kbFrame.intersection(screenSize)
				
				if intersectRect.isNull {
					_kbSize = CGSize(width: screenSize.size.width, height: 0)
				} else {
					_kbSize = intersectRect.size
				}
				
				showLog("UIKeyboard Size : \(_kbSize)")
			}
		}
		
		var topMostController = _textFieldView?.topMostController()
		
		if topMostController == nil {
			topMostController = keyWindow()?.topMostController()
		}
		
		if _kbSize.equalTo(oldKBSize) == false {
			
			if _privateIsKeyboardShowing == true &&
				_textFieldView != nil &&
				_textFieldView?.isAlertViewTextField() == false {
				
				//  keyboard is already showing. adjust frame.
				adjustFrame()
			}
		}
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	@objc internal func keyboardDidShow(_ notification : Notification?) -> Void {
		
		if privateIsEnabled() == false {
			return
		}
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		var topMostController = _textFieldView?.topMostController()
		
		if topMostController == nil {
			topMostController = keyWindow()?.topMostController()
		}
		
		if _textFieldView != nil &&
			(topMostController?.modalPresentationStyle == UIModalPresentationStyle.formSheet || topMostController?.modalPresentationStyle == UIModalPresentationStyle.pageSheet) &&
			_textFieldView?.isAlertViewTextField() == false {
			
			OperationQueue.main.addOperation {
				self.adjustFrame()
			}
		}
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	@objc internal func keyboardWillHide(_ notification : Notification?) -> Void {
		
		if notification != nil {
			_kbShowNotification = nil
		}
		
		_privateIsKeyboardShowing = false
		
		if privateIsEnabled() == false {
			return
		}
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		let info : [AnyHashable: Any]? = (notification as NSNotification?)?.userInfo
		
		if let duration =  (info?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue {
			if duration != 0 {
				_animationDuration = duration
			}
		}
		
		if let lastScrollView = _lastScrollView {
			
			UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
				
				lastScrollView.contentInset = self._startingContentInsets
				lastScrollView.scrollIndicatorInsets = self._startingScrollIndicatorInsets
				
				if lastScrollView.shouldRestoreScrollViewContentOffset == true {
					lastScrollView.contentOffset = self._startingContentOffset
				}
				
				self.showLog("Restoring \(lastScrollView._MKUIDescription()) contentInset to : \(self._startingContentInsets) and contentOffset to : \(self._startingContentOffset)")
				
				var superScrollView : UIScrollView? = lastScrollView
				
				while let scrollView = superScrollView {
					
					let contentSize = CGSize(width: max(scrollView.contentSize.width, scrollView.frame.width), height: max(scrollView.contentSize.height, scrollView.frame.height))
					
					let minimumY = contentSize.height - scrollView.frame.height
					
					if minimumY < scrollView.contentOffset.y {
						scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: minimumY)
						
						self.showLog("Restoring \(scrollView._MKUIDescription()) contentOffset to : \(self._startingContentOffset)")
					}
					
					superScrollView = scrollView.superviewOfClassType(UIScrollView.self) as? UIScrollView
				}
			}) { (finished) -> Void in }
		}
		
		if _topViewBeginRect.equalTo(CGRect.zero) == false {
			
			if let rootViewController = _rootViewController {
				_topViewBeginRect.size = rootViewController.view.frame.size
				
				UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
					
					if let constraint = self._layoutGuideConstraint {
						
						constraint.constant = self._layoutGuideConstraintInitialConstant
						rootViewController.view.setNeedsLayout()
						rootViewController.view.layoutIfNeeded()
					}
					else {
						self.showLog("Restoring \(rootViewController._MKUIDescription()) frame to : \(self._topViewBeginRect)")
						
						rootViewController.view.frame = self._topViewBeginRect
						self._privateMovedDistance = 0
						
						if self.layoutIfNeededOnUpdate == true {
							rootViewController.view.setNeedsLayout()
							rootViewController.view.layoutIfNeeded()
						}
					}
				}) { (finished) -> Void in }
				
				_rootViewController = nil
			}
		} else if let constraint = self._layoutGuideConstraint {
			
			if let rootViewController = _rootViewController {
				
				UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
					
					constraint.constant = self._layoutGuideConstraintInitialConstant
					rootViewController.view.setNeedsLayout()
					rootViewController.view.layoutIfNeeded()
				}) { (finished) -> Void in }
			}
		}
		
		_lastScrollView = nil
		_kbSize = CGSize.zero
		_layoutGuideConstraint = nil
		_layoutGuideConstraintInitialConstant = 0
		_startingContentInsets = UIEdgeInsets.zero
		_startingScrollIndicatorInsets = UIEdgeInsets.zero
		_startingContentOffset = CGPoint.zero
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	@objc internal func keyboardDidHide(_ notification:Notification) {
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		_topViewBeginRect = CGRect.zero
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	@objc internal func textFieldViewDidBeginEditing(_ notification:Notification) {
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		_textFieldView = notification.object as? UIView
		
		if overrideKeyboardAppearance == true {
			
			if let textFieldView = _textFieldView as? UITextField {
				if textFieldView.keyboardAppearance != keyboardAppearance {
					textFieldView.keyboardAppearance = keyboardAppearance
					textFieldView.reloadInputViews()
				}
			} else if  let textFieldView = _textFieldView as? UITextView {
				if textFieldView.keyboardAppearance != keyboardAppearance {
					textFieldView.keyboardAppearance = keyboardAppearance
					textFieldView.reloadInputViews()
				}
			}
		}
		
		if privateIsEnableAutoToolbar() == true {
			if _textFieldView is UITextView == true &&
				_textFieldView?.inputAccessoryView == nil {
				
				UIView.animate(withDuration: 0.00001, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
					
					self.addToolbarIfRequired()
					
				}, completion: { (finished) -> Void in
					self._textFieldView?.reloadInputViews()
				})
			} else {
				addToolbarIfRequired()
			}
		} else {
			removeToolbarIfRequired()
		}
		
		_tapGesture.isEnabled = privateShouldResignOnTouchOutside()
		_textFieldView?.window?.addGestureRecognizer(_tapGesture)
		
		if privateIsEnabled() == true {
			if _topViewBeginRect.equalTo(CGRect.zero) == true {
				if let constraint = _textFieldView?.viewController()?.MKUILayoutGuideConstraint {
					_layoutGuideConstraint = constraint
					_layoutGuideConstraintInitialConstant = constraint.constant
				}
				
				_rootViewController = _textFieldView?.topMostController()
				if _rootViewController == nil {
					_rootViewController = keyWindow()?.topMostController()
				}
				
				if let rootViewController = _rootViewController {
					
					_topViewBeginRect = rootViewController.view.frame
					
					if shouldFixInteractivePopGestureRecognizer == true &&
						rootViewController is UINavigationController &&
						rootViewController.modalPresentationStyle != UIModalPresentationStyle.formSheet &&
						rootViewController.modalPresentationStyle != UIModalPresentationStyle.pageSheet {
						if let window = keyWindow() {
							_topViewBeginRect.origin = CGPoint(x: 0,y: window.frame.size.height-rootViewController.view.frame.size.height)
						} else {
							_topViewBeginRect.origin = CGPoint.zero
						}
					}
					
					showLog("Saving \(rootViewController._MKUIDescription()) beginning frame : \(_topViewBeginRect)")
				}
			}
			
			if _privateIsKeyboardShowing == true &&
				_textFieldView != nil &&
				_textFieldView?.isAlertViewTextField() == false {
				adjustFrame()
			}
		}
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	
	@objc internal func textFieldViewDidEndEditing(_ notification:Notification) {
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		_textFieldView?.window?.removeGestureRecognizer(_tapGesture)
		
		if let textView = _textFieldView as? UITextView {
			
			if isTextViewContentInsetChanged == true {
				
				UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
					
					self.isTextViewContentInsetChanged = false
					
					self.showLog("Restoring \(textView._MKUIDescription()) textView.contentInset to : \(self.startingTextViewContentInsets)")
					
					textView.contentInset = self.startingTextViewContentInsets
					textView.scrollIndicatorInsets = self.startingTextViewScrollIndicatorInsets
					
				}, completion: { (finished) -> Void in })
			}
		}
		
		_textFieldView = nil
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	@objc internal func willChangeStatusBarOrientation(_ notification:Notification) {
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		if let textView = _textFieldView as? UITextView {
			
			if isTextViewContentInsetChanged == true {
				
				UIView.animate(withDuration: _animationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState.union(_animationCurve), animations: { () -> Void in
					
					self.isTextViewContentInsetChanged = false
					
					self.showLog("Restoring \(textView._MKUIDescription()) textView.contentInset to : \(self.startingTextViewContentInsets)")
					
					textView.contentInset = self.startingTextViewContentInsets
					textView.scrollIndicatorInsets = self.startingTextViewScrollIndicatorInsets
					
				}, completion: { (finished) -> Void in })
			}
		}
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	@objc internal func didChangeStatusBarFrame(_ notification : Notification?) -> Void {
		
		let oldStatusBarFrame = _statusBarFrame;
		
		if let newFrame =  ((notification as NSNotification?)?.userInfo?[UIApplicationStatusBarFrameUserInfoKey] as? NSNumber)?.cgRectValue {
			
			_statusBarFrame = newFrame
		}
		
		if privateIsEnabled() == false {
			return
		}
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		if _rootViewController != nil &&
			!_topViewBeginRect.equalTo(_rootViewController!.view.frame) == true {
			
			if let unwrappedRootController = _rootViewController {
				_topViewBeginRect = unwrappedRootController.view.frame
				
				if shouldFixInteractivePopGestureRecognizer == true &&
					unwrappedRootController is UINavigationController &&
					unwrappedRootController.modalPresentationStyle != UIModalPresentationStyle.formSheet &&
					unwrappedRootController.modalPresentationStyle != UIModalPresentationStyle.pageSheet {
					
					if let window = keyWindow() {
						_topViewBeginRect.origin = CGPoint(x: 0,y: window.frame.size.height-unwrappedRootController.view.frame.size.height)
					} else {
						_topViewBeginRect.origin = CGPoint.zero
					}
				}
				
				showLog("Saving \(unwrappedRootController._MKUIDescription()) beginning Frame: \(_topViewBeginRect)")
			} else {
				_topViewBeginRect = CGRect.zero
			}
		}
		
		if _privateIsKeyboardShowing == true &&
			_textFieldView != nil &&
			_statusBarFrame.size.equalTo(oldStatusBarFrame.size) == false &&
			_textFieldView?.isAlertViewTextField() == false {
			
			//  keyboard is already showing. adjust frame.
			adjustFrame()
		}
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	fileprivate func responderViews()-> [UIView]? {
		
		var superConsideredView : UIView?
		for disabledClass in toolbarPreviousNextAllowedClasses {
			
			superConsideredView = _textFieldView?.superviewOfClassType(disabledClass)
			
			if superConsideredView != nil {
				break
			}
		}
		
		if superConsideredView != nil {
			return superConsideredView?.deepResponderViews()
		} else {
			
			if let textFields = _textFieldView?.responderSiblings() {
				
				switch toolbarManageBehaviour {
				case MKUIAutoToolbarManageBehaviour.bySubviews:   return textFields
				case MKUIAutoToolbarManageBehaviour.byTag:    return textFields.sortedArrayByTag()
				case MKUIAutoToolbarManageBehaviour.byPosition:    return textFields.sortedArrayByPosition()
				}
			} else {
				return nil
			}
		}
	}
	
	
	fileprivate func addToolbarIfRequired() {
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		if let siblings = responderViews() {
			
			showLog("Found \(siblings.count) responder sibling(s)")
			
			if (siblings.count == 1 && previousNextDisplayMode == .Default) || previousNextDisplayMode == .alwaysHide {
				
				if let textField = _textFieldView {
					
					var needReload = false;
					
					if textField.responds(to: #selector(setter: UITextField.inputAccessoryView)) {
						
						if textField.inputAccessoryView == nil ||
							textField.inputAccessoryView?.tag == MKUIKeyboardManager.kMKUIPreviousNextButtonToolbarTag {
							
							if textField.inputAccessoryView?.tag == MKUIKeyboardManager.kMKUIPreviousNextButtonToolbarTag {
								needReload = true
							}
							
							if let doneBarButtonItemImage = toolbarDoneBarButtonItemImage {
								textField.addRightButtonOnKeyboardWithImage(doneBarButtonItemImage, target: self, action: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
							}
							else if let doneBarButtonItemText = toolbarDoneBarButtonItemText {
								textField.addRightButtonOnKeyboardWithText(doneBarButtonItemText, target: self, action: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
							} else {
								textField.addDoneOnKeyboardWithTarget(self, action: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
							}
							textField.inputAccessoryView?.tag = MKUIKeyboardManager.kMKUIDoneButtonToolbarTag
						}
						else if let toolbar = textField.inputAccessoryView as? MKUIToolbar {
							
							if toolbar.tag == MKUIKeyboardManager.kMKUIDoneButtonToolbarTag {
								if textField.inputAccessoryView?.tag == MKUIKeyboardManager.kMKUIDoneButtonToolbarTag {
									
									if let doneBarButtonItemImage = toolbarDoneBarButtonItemImage {
										if toolbar.doneImage?.isEqual(doneBarButtonItemImage) == false {
											textField.addRightButtonOnKeyboardWithImage(doneBarButtonItemImage, target: self, action: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
											needReload = true
										}
									}
									else if let doneBarButtonItemText = toolbarDoneBarButtonItemText {
										if toolbar.doneTitle != doneBarButtonItemText {
											textField.addRightButtonOnKeyboardWithText(doneBarButtonItemText, target: self, action: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
											needReload = true
										}
									} else if (toolbarDoneBarButtonItemText == nil && toolbar.doneTitle != nil) ||
										(toolbarDoneBarButtonItemImage == nil && toolbar.doneImage != nil) {
										textField.addDoneOnKeyboardWithTarget(self, action: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
										needReload = true
									}
									textField.inputAccessoryView?.tag = MKUIKeyboardManager.kMKUIDoneButtonToolbarTag
								}
							}
						}
					}
					
					if textField.inputAccessoryView is MKUIToolbar &&
						textField.inputAccessoryView?.tag == MKUIKeyboardManager.kMKUIDoneButtonToolbarTag {
						
						let toolbar = textField.inputAccessoryView as! MKUIToolbar
						
						if let _textField = textField as? UITextField {
							
							switch _textField.keyboardAppearance {
								
							case UIKeyboardAppearance.dark:
								toolbar.barStyle = UIBarStyle.black
								toolbar.tintColor = UIColor.white
							default:
								toolbar.barStyle = UIBarStyle.default
								
								if shouldToolbarUsesTextFieldTintColor {
									toolbar.tintColor = _textField.tintColor
								} else if let tintColor = toolbarTintColor {
									toolbar.tintColor = tintColor
								} else {
									toolbar.tintColor = UIColor.black
								}
							}
						} else if let _textView = textField as? UITextView {
							
							switch _textView.keyboardAppearance {
								
							case UIKeyboardAppearance.dark:
								toolbar.barStyle = UIBarStyle.black
								toolbar.tintColor = UIColor.white
							default:
								toolbar.barStyle = UIBarStyle.default
								
								if shouldToolbarUsesTextFieldTintColor {
									toolbar.tintColor = _textView.tintColor
								} else if let tintColor = toolbarTintColor {
									toolbar.tintColor = tintColor
								} else {
									toolbar.tintColor = UIColor.black
								}
							}
						}
						
						if shouldShowTextFieldPlaceholder == true &&
							textField.shouldHidePlaceholderText == false {
							
							if toolbar.title == nil ||
								toolbar.title != textField.drawingPlaceholderText {
								toolbar.title = textField.drawingPlaceholderText
							}
							
							if placeholderFont != nil {
								toolbar.titleFont = placeholderFont
							}
						} else {
							
							toolbar.title = nil
						}
					}
					
					if needReload {
						textField.reloadInputViews()
					}
				}
			} else if (siblings.count > 1 && previousNextDisplayMode == .Default) || previousNextDisplayMode == .alwaysShow {
				for textField in siblings {
					
					var needReload = false;
					if textField.responds(to: #selector(setter: UITextField.inputAccessoryView)) &&
						(textField.inputAccessoryView == nil || textField.inputAccessoryView?.tag == MKUIKeyboardManager.kMKUIDoneButtonToolbarTag) {
						
						if textField.inputAccessoryView == nil ||
							textField.inputAccessoryView?.tag == MKUIKeyboardManager.kMKUIDoneButtonToolbarTag {
							
							if textField.inputAccessoryView?.tag == MKUIKeyboardManager.kMKUIDoneButtonToolbarTag {
								needReload = true
							}
							
							if let doneBarButtonItemImage = toolbarDoneBarButtonItemImage {
								textField.addPreviousNextRightOnKeyboardWithTarget(self, rightButtonImage: doneBarButtonItemImage, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), rightButtonAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
							}
							else if let doneBarButtonItemText = toolbarDoneBarButtonItemText {
								textField.addPreviousNextRightOnKeyboardWithTarget(self, rightButtonTitle: doneBarButtonItemText, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), rightButtonAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
							} else {
								textField.addPreviousNextDoneOnKeyboardWithTarget(self, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), doneAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
							}
						}
						else if let toolbar = textField.inputAccessoryView as? MKUIToolbar {
							
							if textField.inputAccessoryView?.tag == MKUIKeyboardManager.kMKUIPreviousNextButtonToolbarTag {
								if let doneBarButtonItemImage = toolbarDoneBarButtonItemImage {
									if toolbar.doneImage?.isEqual(doneBarButtonItemImage) == false {
										textField.addPreviousNextRightOnKeyboardWithTarget(self, rightButtonImage: toolbarDoneBarButtonItemImage!, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), rightButtonAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
										needReload = true
									}
								}
								else if let doneBarButtonItemText = toolbarDoneBarButtonItemText {
									if toolbar.doneTitle != doneBarButtonItemText {
										textField.addPreviousNextRightOnKeyboardWithTarget(self, rightButtonTitle: toolbarDoneBarButtonItemText!, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), rightButtonAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
										needReload = true
									}
								} else if (toolbarDoneBarButtonItemText == nil && toolbar.doneTitle != nil) ||
									(toolbarDoneBarButtonItemImage == nil && toolbar.doneImage != nil) {
									textField.addPreviousNextDoneOnKeyboardWithTarget(self, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), doneAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: shouldShowTextFieldPlaceholder)
									needReload = true
								}
							}
						}
						
						textField.inputAccessoryView?.tag = MKUIKeyboardManager.kMKUIPreviousNextButtonToolbarTag //  (Bug ID: #78)
					}
					
					if textField.inputAccessoryView is MKUIToolbar &&
						textField.inputAccessoryView?.tag == MKUIKeyboardManager.kMKUIPreviousNextButtonToolbarTag {
						
						let toolbar = textField.inputAccessoryView as! MKUIToolbar
						
						if let _textField = textField as? UITextField {
							
							switch _textField.keyboardAppearance {
								
							case UIKeyboardAppearance.dark:
								toolbar.barStyle = UIBarStyle.black
								toolbar.tintColor = UIColor.white
							default:
								toolbar.barStyle = UIBarStyle.default
								
								if shouldToolbarUsesTextFieldTintColor {
									toolbar.tintColor = _textField.tintColor
								} else if let tintColor = toolbarTintColor {
									toolbar.tintColor = tintColor
								} else {
									toolbar.tintColor = UIColor.black
								}
							}
						} else if let _textView = textField as? UITextView {
							
							switch _textView.keyboardAppearance {
								
							case UIKeyboardAppearance.dark:
								toolbar.barStyle = UIBarStyle.black
								toolbar.tintColor = UIColor.white
							default:
								toolbar.barStyle = UIBarStyle.default
								
								if shouldToolbarUsesTextFieldTintColor {
									toolbar.tintColor = _textView.tintColor
								} else if let tintColor = toolbarTintColor {
									toolbar.tintColor = tintColor
								} else {
									toolbar.tintColor = UIColor.black
								}
							}
						}
						
						if shouldShowTextFieldPlaceholder == true &&
							textField.shouldHidePlaceholderText == false {
							
							if toolbar.title == nil ||
								toolbar.title != textField.drawingPlaceholderText {
								toolbar.title = textField.drawingPlaceholderText
							}
							
							if placeholderFont != nil {
								toolbar.titleFont = placeholderFont
							}
						}
						else {
							
							toolbar.title = nil
						}
						
						if siblings[0] == textField {
							if (siblings.count == 1) {
								textField.setEnablePrevious(false, isNextEnabled: false)
							} else {
								textField.setEnablePrevious(false, isNextEnabled: true)
							}
						} else if siblings.last  == textField {
							textField.setEnablePrevious(true, isNextEnabled: false)
						} else {
							textField.setEnablePrevious(true, isNextEnabled: true)
						}
					}
					
					if needReload {
						textField.reloadInputViews()
					}
				}
			}
		}
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	
	fileprivate func removeToolbarIfRequired() {    //  (Bug ID: #18)
		
		let startTime = CACurrentMediaTime()
		showLog("****** \(#function) started ******")
		
		if let siblings = responderViews() {
			
			showLog("Found \(siblings.count) responder sibling(s)")
			
			for view in siblings {
				
				if let toolbar = view.inputAccessoryView as? MKUIToolbar {
					
					if view.responds(to: #selector(setter: UITextField.inputAccessoryView)) &&
						(toolbar.tag == MKUIKeyboardManager.kMKUIDoneButtonToolbarTag || toolbar.tag == MKUIKeyboardManager.kMKUIPreviousNextButtonToolbarTag) {
						
						if let textField = view as? UITextField {
							textField.inputAccessoryView = nil
						} else if let textView = view as? UITextView {
							textView.inputAccessoryView = nil
						}
					}
				}
			}
		}
		
		let elapsedTime = CACurrentMediaTime() - startTime
		showLog("****** \(#function) ended: \(elapsedTime) seconds ******")
	}
	
	
	open func reloadInputViews() {
		if privateIsEnableAutoToolbar() == true {
			self.addToolbarIfRequired()
		} else {
			self.removeToolbarIfRequired()
		}
	}
	
	open var enableDebugging = false
	
	fileprivate func showLog(_ logString: String) {
		if enableDebugging {
			print("MKUIKeyboardManager: " + logString)
		}
	}
}

