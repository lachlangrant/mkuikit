//
//  MKUI_UIViewController+Additions.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import UIKit


private var kMKUILayoutGuideConstraint = "kMKUILayoutGuideConstraint"


public extension UIViewController {
	
	/**
	To set customized distance from keyboard for textField/textView. Can't be less than zero
	*/
	@IBOutlet public var MKUILayoutGuideConstraint: NSLayoutConstraint? {
		get {
			
			return objc_getAssociatedObject(self, kMKUILayoutGuideConstraint) as? NSLayoutConstraint
		}
		
		set(newValue) {
			objc_setAssociatedObject(self, kMKUILayoutGuideConstraint, newValue,objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
		}
	}
}
