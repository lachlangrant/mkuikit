//
//  MKUI_NSArray+Sort.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import UIKit

internal extension Array {
	internal func sortedArrayByTag() -> [Element] {
		
		return sorted(by: { (obj1: Element, obj2: Element) -> Bool in
			let view1 = obj1 as! UIView
			let view2 = obj2 as! UIView
			
			return (view1.tag < view2.tag)
		})
	}
	
	internal func sortedArrayByPosition() -> [Element] {
		
		return sorted(by: { (obj1 : Element, obj2 : Element) -> Bool in
			
			let view1 = obj1 as! UIView
			let view2 = obj2 as! UIView
			
			let x1 = view1.frame.minX
			let y1 = view1.frame.minY
			let x2 = view2.frame.minX
			let y2 = view2.frame.minY
			
			if y1 != y2 {
				return y1 < y2
			} else {
				return x1 < x2
			}
		})
	}
}
