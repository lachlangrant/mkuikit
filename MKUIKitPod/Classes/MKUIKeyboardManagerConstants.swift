//
//  MKUIKeyboardManagerConstants.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import Foundation

public enum MKUIAutoToolbarManageBehaviour: Int {
	case bySubviews
	case byTag
	case byPosition
}

public enum MKUIPreviousNextDisplayMode: Int {
	case Default
	case alwaysHide
	case alwaysShow
}

public enum MKUILayoutGuidePosition: Int {
	case none
	case top
	case bottom
}
