//
//  MKUITitleBarButtonItem.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import UIKit

private var kMKUIBarTitleInvocationTarget     = "kMKUIBarTitleInvocationTarget"
private var kMKUIBarTitleInvocationSelector   = "kMKUIBarTitleInvocationSelector"

open class MKUITitleBarButtonItem: MKUIBarButtonItem {
	
	fileprivate var _titleButton : UIButton?
	fileprivate var _titleView : UIView?
	
	open var font: UIFont? {
		didSet {
			if let unFont = font {
				_titleButton?.titleLabel?.font = unFont
			} else {
				_titleButton?.titleLabel?.font = UIFont.systemFont(ofSize: 13)
			}
		}
	}
	
	override open var title: String? {
		didSet {
			_titleButton?.setTitle(title, for: UIControlState())
		}
	}
	
	open var selectableTextColor: UIColor? {
		
		didSet {
			if let color = selectableTextColor {
				_titleButton?.setTitleColor(color, for: UIControlState())
			} else {
				_titleButton?.setTitleColor(UIColor.init(red: 0.0, green: 0.5, blue: 1.0, alpha: 1), for: UIControlState())
			}
		}
	}
	
	open var titleInvocation: (target: AnyObject?, action: Selector?) {
		
		get {
			let target = objc_getAssociatedObject(self, kMKUIBarTitleInvocationTarget) as AnyObject?
			
			var action: Selector?
			
			if let selectorString = objc_getAssociatedObject(self, kMKUIBarTitleInvocationSelector) as? String {
				action = NSSelectorFromString(selectorString)
			}
			
			return (target: target, action: action)
		}
		
		set(newValue) {
			objc_setAssociatedObject(self, kMKUIBarTitleInvocationTarget, newValue.target, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			
			if let unwrappedSelector = newValue.action {
				objc_setAssociatedObject(self, kMKUIBarTitleInvocationSelector, NSStringFromSelector(unwrappedSelector), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			} else {
				objc_setAssociatedObject(self, kMKUIBarTitleInvocationSelector, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			}
			
			if (newValue.target == nil || newValue.action == nil)
			{
				self.isEnabled = false
				_titleButton?.isEnabled = false
				_titleButton?.removeTarget(nil, action: nil, for: .touchUpInside)
			}
			else
			{
				self.isEnabled = true
				_titleButton?.isEnabled = true
				_titleButton?.addTarget(newValue.target, action: newValue.action!, for: .touchUpInside)
			}
		}
	}
	
	override init() {
		super.init()
	}
	
	convenience init(title: String?) {
		self.init()
		_titleView = UIView()
		_titleView?.backgroundColor = UIColor.clear
		_titleView?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
		
		_titleButton = UIButton(type: .system)
		_titleButton?.isEnabled = false
		_titleButton?.titleLabel?.numberOfLines = 3
		_titleButton?.setTitleColor(UIColor.lightGray, for:.disabled)
		_titleButton?.setTitleColor(UIColor.init(red: 0.0, green: 0.5, blue: 1.0, alpha: 1), for:UIControlState())
		_titleButton?.backgroundColor = UIColor.clear
		_titleButton?.titleLabel?.textAlignment = .center
		_titleButton?.setTitle(title, for: UIControlState())
		_titleButton?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
		font = UIFont.systemFont(ofSize: 13.0)
		_titleButton?.titleLabel?.font = self.font
		_titleView?.addSubview(_titleButton!)
		customView = _titleView
	}
	
	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
