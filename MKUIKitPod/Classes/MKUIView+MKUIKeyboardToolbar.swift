//
//  MKUIView+MKUIKeyboardToolbar.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
	switch (lhs, rhs) {
	case let (l?, r?):
		return l < r
	case (nil, _?):
		return true
	default:
		return false
	}
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
	switch (lhs, rhs) {
	case let (l?, r?):
		return l > r
	default:
		return rhs < lhs
	}
}


private var kMKUIShouldHidePlaceholderText    = "kMKUIShouldHidePlaceholderText"
private var kMKUIPlaceholderText              = "kMKUIPlaceholderText"


private var kMKUITitleInvocationTarget        = "kMKUITitleInvocationTarget"
private var kMKUITitleInvocationSelector      = "kMKUITitleInvocationSelector"

private var kMKUIPreviousInvocationTarget     = "kMKUIPreviousInvocationTarget"
private var kMKUIPreviousInvocationSelector   = "kMKUIPreviousInvocationSelector"
private var kMKUINextInvocationTarget         = "kMKUINextInvocationTarget"
private var kMKUINextInvocationSelector       = "kMKUINextInvocationSelector"
private var kMKUIDoneInvocationTarget         = "kMKUIDoneInvocationTarget"
private var kMKUIDoneInvocationSelector       = "kMKUIDoneInvocationSelector"


public extension UIView {
	
	public var shouldHidePlaceholderText: Bool {
		get {
			let aValue: AnyObject? = objc_getAssociatedObject(self, &kMKUIShouldHidePlaceholderText) as AnyObject?
			
			if let unwrapedValue = aValue as? Bool {
				return unwrapedValue
			} else {
				return false
			}
		}
		set(newValue) {
			objc_setAssociatedObject(self, &kMKUIShouldHidePlaceholderText, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			
			if let toolbar = self.inputAccessoryView as? MKUIToolbar {
				if self.responds(to: #selector(getter: UITextField.placeholder)) {
					toolbar.title = self.drawingPlaceholderText
				}
			}
		}
	}

	public var placeholderText: String? {
		get {
			let aValue = objc_getAssociatedObject(self, &kMKUIPlaceholderText) as? String
			
			return aValue
		}
		set(newValue) {
			objc_setAssociatedObject(self, &kMKUIPlaceholderText, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			
			if let toolbar = self.inputAccessoryView as? MKUIToolbar {
				if self.responds(to: #selector(getter: UITextField.placeholder)) {
					toolbar.title = self.drawingPlaceholderText
				}
			}
		}
	}

	public var drawingPlaceholderText: String? {
		
		if (self.shouldHidePlaceholderText)
		{
			return nil
		}
		else if (self.placeholderText?.isEmpty == false) {
			return self.placeholderText
		}
		else if self.responds(to: #selector(getter: UITextField.placeholder)) {
			
			if let textField = self as? UITextField {
				return textField.placeholder
			} else if let textView = self as? MKUITextView {
				return textView.placeholder
			} else {
				return nil
			}
		}
		else {
			return nil
		}
	}

	public func setTitleTarget(_ target: AnyObject?, action: Selector?) {
		titleInvocation = (target, action)
	}

	public var titleInvocation : (target: AnyObject?, action: Selector?) {
		get {
			let target: AnyObject? = objc_getAssociatedObject(self, &kMKUITitleInvocationTarget) as AnyObject?
			var action : Selector?
			
			if let selectorString = objc_getAssociatedObject(self, &kMKUITitleInvocationSelector) as? String {
				action = NSSelectorFromString(selectorString)
			}
			
			return (target: target, action: action)
		}
		set(newValue) {
			objc_setAssociatedObject(self, &kMKUITitleInvocationTarget, newValue.target, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			
			if let unwrappedSelector = newValue.action {
				objc_setAssociatedObject(self, &kMKUITitleInvocationSelector, NSStringFromSelector(unwrappedSelector), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			} else {
				objc_setAssociatedObject(self, &kMKUITitleInvocationSelector, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			}
		}
	}

	public func setCustomPreviousTarget(_ target: AnyObject?, action: Selector?) {
		previousInvocation = (target, action)
	}

	public func setCustomNextTarget(_ target: AnyObject?, action: Selector?) {
		nextInvocation = (target, action)
	}

	public func setCustomDoneTarget(_ target: AnyObject?, action: Selector?) {
		doneInvocation = (target, action)
	}

	public var previousInvocation : (target: AnyObject?, action: Selector?) {
		get {
			let target: AnyObject? = objc_getAssociatedObject(self, &kMKUIPreviousInvocationTarget) as AnyObject?
			var action : Selector?
			
			if let selectorString = objc_getAssociatedObject(self, &kMKUIPreviousInvocationSelector) as? String {
				action = NSSelectorFromString(selectorString)
			}
			
			return (target: target, action: action)
		}
		set(newValue) {
			objc_setAssociatedObject(self, &kMKUIPreviousInvocationTarget, newValue.target, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			
			if let unwrappedSelector = newValue.action {
				objc_setAssociatedObject(self, &kMKUIPreviousInvocationSelector, NSStringFromSelector(unwrappedSelector), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			} else {
				objc_setAssociatedObject(self, &kMKUIPreviousInvocationSelector, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			}
		}
	}

	public var nextInvocation : (target: AnyObject?, action: Selector?) {
		get {
			let target: AnyObject? = objc_getAssociatedObject(self, &kMKUINextInvocationTarget) as AnyObject?
			var action : Selector?
			
			if let selectorString = objc_getAssociatedObject(self, &kMKUINextInvocationSelector) as? String {
				action = NSSelectorFromString(selectorString)
			}
			
			return (target: target, action: action)
		}
		set(newValue) {
			objc_setAssociatedObject(self, &kMKUINextInvocationTarget, newValue.target, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			
			if let unwrappedSelector = newValue.action {
				objc_setAssociatedObject(self, &kMKUINextInvocationSelector, NSStringFromSelector(unwrappedSelector), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			} else {
				objc_setAssociatedObject(self, &kMKUINextInvocationSelector, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			}
		}
	}

	public var doneInvocation : (target: AnyObject?, action: Selector?) {
		get {
			let target: AnyObject? = objc_getAssociatedObject(self, &kMKUIDoneInvocationTarget) as AnyObject?
			var action : Selector?
			
			if let selectorString = objc_getAssociatedObject(self, &kMKUIDoneInvocationSelector) as? String {
				action = NSSelectorFromString(selectorString)
			}
			
			return (target: target, action: action)
		}
		set(newValue) {
			objc_setAssociatedObject(self, &kMKUIDoneInvocationTarget, newValue.target, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			
			if let unwrappedSelector = newValue.action {
				objc_setAssociatedObject(self, &kMKUIDoneInvocationSelector, NSStringFromSelector(unwrappedSelector), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			} else {
				objc_setAssociatedObject(self, &kMKUIDoneInvocationSelector, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
			}
		}
	}

	fileprivate static func flexibleBarButtonItem () -> MKUIBarButtonItem {
		
		struct Static {
			static let nilButton = MKUIBarButtonItem(barButtonSystemItem:UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
		}
		
		return Static.nilButton
	}

	public func addDoneOnKeyboardWithTarget(_ target : AnyObject?, action : Selector) {
		
		addDoneOnKeyboardWithTarget(target, action: action, titleText: nil)
	}

	public func addDoneOnKeyboardWithTarget (_ target : AnyObject?, action : Selector, titleText: String?) {
		
				if self.responds(to: #selector(setter: UITextField.inputAccessoryView)) {
			
						let toolbar = MKUIToolbar()
			
			var items : [UIBarButtonItem] = []
			
						let title = MKUITitleBarButtonItem(title: shouldHidePlaceholderText == true ? nil : titleText)
			items.append(title)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let doneButton = MKUIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: target, action: action)
			items.append(doneButton)
			
						toolbar.items = items
			toolbar.toolbarTitleInvocation = self.titleInvocation
			
						if let textField = self as? UITextField {
				textField.inputAccessoryView = toolbar
				
				switch textField.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			} else if let textView = self as? UITextView {
				textView.inputAccessoryView = toolbar
				
				switch textView.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			}
		}
	}
	
	public func addDoneOnKeyboardWithTarget (_ target : AnyObject?, action : Selector, shouldShowPlaceholder: Bool) {
		
		var title : String?
		
		if shouldShowPlaceholder == true {
			title = self.drawingPlaceholderText
		}
		
		addDoneOnKeyboardWithTarget(target, action: action, titleText: title)
	}

	public func addRightButtonOnKeyboardWithImage (_ image : UIImage, target : AnyObject?, action : Selector, titleText: String?) {
		
				if self.responds(to: #selector(setter: UITextField.inputAccessoryView)) {
			
						let toolbar = MKUIToolbar()
			toolbar.doneImage = image
			
			var items : [UIBarButtonItem] = []
			
						let title = MKUITitleBarButtonItem(title: shouldHidePlaceholderText == true ? nil : titleText)
			items.append(title)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let doneButton = MKUIBarButtonItem(image: image, style: UIBarButtonItemStyle.done, target: target, action: action)
			doneButton.accessibilityLabel = "Toolbar Done Button"
			items.append(doneButton)
			
						toolbar.items = items
			toolbar.toolbarTitleInvocation = self.titleInvocation
			
						if let textField = self as? UITextField {
				textField.inputAccessoryView = toolbar
				
				switch textField.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			} else if let textView = self as? UITextView {
				textView.inputAccessoryView = toolbar
				
				switch textView.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			}
		}
	}

	public func addRightButtonOnKeyboardWithImage (_ image : UIImage, target : AnyObject?, action : Selector, shouldShowPlaceholder: Bool) {
		
		var title : String?
		
		if shouldShowPlaceholder == true {
			title = self.drawingPlaceholderText
		}
		
		addRightButtonOnKeyboardWithImage(image, target: target, action: action, titleText: title)
	}

	public func addRightButtonOnKeyboardWithText (_ text : String, target : AnyObject?, action : Selector) {
		
		addRightButtonOnKeyboardWithText(text, target: target, action: action, titleText: nil)
	}

	public func addRightButtonOnKeyboardWithText (_ text : String, target : AnyObject?, action : Selector, titleText: String?) {
		
				if self.responds(to: #selector(setter: UITextField.inputAccessoryView)) {
			
						let toolbar = MKUIToolbar()
			toolbar.doneTitle = text
			
			var items : [UIBarButtonItem] = []
			
						let title = MKUITitleBarButtonItem(title: shouldHidePlaceholderText == true ? nil : titleText)
			items.append(title)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let doneButton = MKUIBarButtonItem(title: text, style: UIBarButtonItemStyle.done, target: target, action: action)
			items.append(doneButton)
			
						toolbar.items = items
			toolbar.toolbarTitleInvocation = self.titleInvocation
			
						if let textField = self as? UITextField {
				textField.inputAccessoryView = toolbar
				
				switch textField.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			} else if let textView = self as? UITextView {
				textView.inputAccessoryView = toolbar
				
				switch textView.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			}
		}
	}

	public func addRightButtonOnKeyboardWithText (_ text : String, target : AnyObject?, action : Selector, shouldShowPlaceholder: Bool) {
		
		var title : String?
		
		if shouldShowPlaceholder == true {
			title = self.drawingPlaceholderText
		}
		
		addRightButtonOnKeyboardWithText(text, target: target, action: action, titleText: title)
	}

	public func addCancelDoneOnKeyboardWithTarget (_ target : AnyObject?, cancelAction : Selector, doneAction : Selector) {
		
		addCancelDoneOnKeyboardWithTarget(target, cancelAction: cancelAction, doneAction: doneAction, titleText: nil)
	}

	public func addCancelDoneOnKeyboardWithTarget (_ target : AnyObject?, cancelAction : Selector, doneAction : Selector, titleText: String?) {
		
				if self.responds(to: #selector(setter: UITextField.inputAccessoryView)) {
						let toolbar = MKUIToolbar()
			
			var items : [UIBarButtonItem] = []
			
						let cancelButton = MKUIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: target, action: cancelAction)
			items.append(cancelButton)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let title = MKUITitleBarButtonItem(title: shouldHidePlaceholderText == true ? nil : titleText)
			items.append(title)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let doneButton = MKUIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: target, action: doneAction)
			items.append(doneButton)
			
						toolbar.items = items
			toolbar.toolbarTitleInvocation = self.titleInvocation
			
						if let textField = self as? UITextField {
				textField.inputAccessoryView = toolbar
				
				switch textField.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			} else if let textView = self as? UITextView {
				textView.inputAccessoryView = toolbar
				
				switch textView.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			}
		}
	}

	public func addCancelDoneOnKeyboardWithTarget (_ target : AnyObject?, cancelAction : Selector, doneAction : Selector, shouldShowPlaceholder: Bool) {
		
		var title : String?
		
		if shouldShowPlaceholder == true {
			title = self.drawingPlaceholderText
		}
		
		addCancelDoneOnKeyboardWithTarget(target, cancelAction: cancelAction, doneAction: doneAction, titleText: title)
	}

	public func addRightLeftOnKeyboardWithTarget( _ target : AnyObject?, leftButtonTitle : String, rightButtonTitle : String, rightButtonAction : Selector, leftButtonAction : Selector) {
		
		addRightLeftOnKeyboardWithTarget(target, leftButtonTitle: leftButtonTitle, rightButtonTitle: rightButtonTitle, rightButtonAction: rightButtonAction, leftButtonAction: leftButtonAction, titleText: nil)
	}

	public func addRightLeftOnKeyboardWithTarget( _ target : AnyObject?, leftButtonTitle : String, rightButtonTitle : String, rightButtonAction : Selector, leftButtonAction : Selector, titleText: String?) {
		
				if self.responds(to: #selector(setter: UITextField.inputAccessoryView)) {
						let toolbar = MKUIToolbar()
			toolbar.doneTitle = rightButtonTitle
			
			var items : [UIBarButtonItem] = []
			
						let cancelButton = MKUIBarButtonItem(title: leftButtonTitle, style: UIBarButtonItemStyle.plain, target: target, action: leftButtonAction)
			items.append(cancelButton)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let title = MKUITitleBarButtonItem(title: shouldHidePlaceholderText == true ? nil : titleText)
			items.append(title)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let doneButton = MKUIBarButtonItem(title: rightButtonTitle, style: UIBarButtonItemStyle.done, target: target, action: rightButtonAction)
			items.append(doneButton)
			
						toolbar.items = items
			toolbar.toolbarTitleInvocation = self.titleInvocation
			
						if let textField = self as? UITextField {
				textField.inputAccessoryView = toolbar
				
				switch textField.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			} else if let textView = self as? UITextView {
				textView.inputAccessoryView = toolbar
				
				switch textView.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			}
		}
	}

	public func addRightLeftOnKeyboardWithTarget( _ target : AnyObject?, leftButtonTitle : String, rightButtonTitle : String, rightButtonAction : Selector, leftButtonAction : Selector, shouldShowPlaceholder: Bool) {
		
		var title : String?
		
		if shouldShowPlaceholder == true {
			title = self.drawingPlaceholderText
		}
		
		addRightLeftOnKeyboardWithTarget(target, leftButtonTitle: leftButtonTitle, rightButtonTitle: rightButtonTitle, rightButtonAction: rightButtonAction, leftButtonAction: leftButtonAction, titleText: title)
	}

	public func addPreviousNextDoneOnKeyboardWithTarget ( _ target : AnyObject?, previousAction : Selector, nextAction : Selector, doneAction : Selector) {
		
		addPreviousNextDoneOnKeyboardWithTarget(target, previousAction: previousAction, nextAction: nextAction, doneAction: doneAction, titleText: nil)
	}

	public func addPreviousNextDoneOnKeyboardWithTarget ( _ target : AnyObject?, previousAction : Selector, nextAction : Selector, doneAction : Selector,  titleText: String?) {
		
				if self.responds(to: #selector(setter: UITextField.inputAccessoryView)) {
						let toolbar = MKUIToolbar()
			
			var items : [UIBarButtonItem] = []
			
			let prev : MKUIBarButtonItem
			let next : MKUIBarButtonItem
			
			var imageLeftArrow : UIImage!
			var imageRightArrow : UIImage!
			
			if #available(iOS 10.0, *) {
				imageLeftArrow = UIImage(named: "MKUIButtonBarArrowUp")
				imageRightArrow = UIImage(named: "MKUIButtonBarArrowDown")
			} else {
				imageLeftArrow = UIImage(named: "MKUIButtonBarArrowLeft")
				imageRightArrow = UIImage(named: "MKUIButtonBarArrowRight")
			}
			
			if #available(iOS 9.0, *) {
				imageLeftArrow = imageLeftArrow?.imageFlippedForRightToLeftLayoutDirection()
				imageRightArrow = imageRightArrow?.imageFlippedForRightToLeftLayoutDirection()
			}
			
			prev = MKUIBarButtonItem(image: imageLeftArrow, style: UIBarButtonItemStyle.plain, target: target, action: previousAction)
			prev.accessibilityLabel = "Toolbar Previous Button"
			
			next = MKUIBarButtonItem(image: imageRightArrow, style: UIBarButtonItemStyle.plain, target: target, action: nextAction)
			next.accessibilityLabel = "Toolbar Next Button"
			
						items.append(prev)
			
						let fixed = MKUIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
			if #available(iOS 10.0, *) {
				fixed.width = 6
			} else {
				fixed.width = 20
			}
			items.append(fixed)
			
						items.append(next)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let title = MKUITitleBarButtonItem(title: shouldHidePlaceholderText == true ? nil : titleText)
			items.append(title)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let doneButton = MKUIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: target, action: doneAction)
			items.append(doneButton)
			
						toolbar.items = items
			toolbar.toolbarTitleInvocation = self.titleInvocation
			
						if let textField = self as? UITextField {
				textField.inputAccessoryView = toolbar
				
				switch textField.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			} else if let textView = self as? UITextView {
				textView.inputAccessoryView = toolbar
				
				switch textView.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			}
		}
	}

	public func addPreviousNextDoneOnKeyboardWithTarget ( _ target : AnyObject?, previousAction : Selector, nextAction : Selector, doneAction : Selector, shouldShowPlaceholder: Bool) {
		
		var title : String?
		
		if shouldShowPlaceholder == true {
			title = self.drawingPlaceholderText
		}
		
		addPreviousNextDoneOnKeyboardWithTarget(target, previousAction: previousAction, nextAction: nextAction, doneAction: doneAction, titleText: title)
	}

	public func addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonImage : UIImage, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector, titleText : String?) {
		
				if self.responds(to: #selector(setter: UITextField.inputAccessoryView)) {
						let toolbar = MKUIToolbar()
			toolbar.doneImage = rightButtonImage
			
			var items : [UIBarButtonItem] = []
			
			let prev : MKUIBarButtonItem
			let next : MKUIBarButtonItem
			
						var bundle = Bundle(for: MKUIKeyboardManager.self)
			
			if let resourcePath = bundle.path(forResource: "MKUIKeyboardManager", ofType: "bundle") {
				if let resourcesBundle = Bundle(path: resourcePath) {
					bundle = resourcesBundle
				}
			}
			
			var imageLeftArrow : UIImage!
			var imageRightArrow : UIImage!
			
			if #available(iOS 10.0, *) {
				imageLeftArrow = UIImage(named: "MKUIButtonBarArrowUp", in: bundle, compatibleWith: nil)
				imageRightArrow = UIImage(named: "MKUIButtonBarArrowDown", in: bundle, compatibleWith: nil)
			} else {
				imageLeftArrow = UIImage(named: "MKUIButtonBarArrowLeft", in: bundle, compatibleWith: nil)
				imageRightArrow = UIImage(named: "MKUIButtonBarArrowRight", in: bundle, compatibleWith: nil)
			}
			
						if #available(iOS 9.0, *) {
				imageLeftArrow = imageLeftArrow?.imageFlippedForRightToLeftLayoutDirection()
				imageRightArrow = imageRightArrow?.imageFlippedForRightToLeftLayoutDirection()
			}
			
			prev = MKUIBarButtonItem(image: imageLeftArrow, style: UIBarButtonItemStyle.plain, target: target, action: previousAction)
			prev.accessibilityLabel = "Toolbar Previous Button"
			
			next = MKUIBarButtonItem(image: imageRightArrow, style: UIBarButtonItemStyle.plain, target: target, action: nextAction)
			next.accessibilityLabel = "Toolbar Next Button"
			
						items.append(prev)
			
						let fixed = MKUIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
			if #available(iOS 10.0, *) {
				fixed.width = 6
			} else {
				fixed.width = 20
			}
			items.append(fixed)
			
						items.append(next)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let title = MKUITitleBarButtonItem(title: shouldHidePlaceholderText == true ? nil : titleText)
			items.append(title)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let doneButton = MKUIBarButtonItem(image: rightButtonImage, style: UIBarButtonItemStyle.done, target: target, action: rightButtonAction)
			doneButton.accessibilityLabel = "Toolbar Done Button"
			items.append(doneButton)
			
						toolbar.items = items
			toolbar.toolbarTitleInvocation = self.titleInvocation
			
						if let textField = self as? UITextField {
				textField.inputAccessoryView = toolbar
				
				switch textField.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			} else if let textView = self as? UITextView {
				textView.inputAccessoryView = toolbar
				
				switch textView.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			}
		}
	}

	public func addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonImage : UIImage, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector, shouldShowPlaceholder : Bool) {
		
		var title : String?
		
		if shouldShowPlaceholder == true {
			title = self.drawingPlaceholderText
		}
		
		addPreviousNextRightOnKeyboardWithTarget(target, rightButtonImage: rightButtonImage, previousAction: previousAction, nextAction: nextAction, rightButtonAction: rightButtonAction, titleText: title)
	}

	public func addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonTitle : String, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector) {
		
		addPreviousNextRightOnKeyboardWithTarget(target, rightButtonTitle: rightButtonTitle, previousAction: previousAction, nextAction: nextAction, rightButtonAction: rightButtonAction, titleText: nil)
	}

	public func addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonTitle : String, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector, titleText : String?) {
		
				if self.responds(to: #selector(setter: UITextField.inputAccessoryView)) {
						let toolbar = MKUIToolbar()
			toolbar.doneTitle = rightButtonTitle
			
			var items : [UIBarButtonItem] = []
			
			let prev : MKUIBarButtonItem
			let next : MKUIBarButtonItem
			
						var bundle = Bundle(for: MKUIKeyboardManager.self)
			
			if let resourcePath = bundle.path(forResource: "MKUIKeyboardManager", ofType: "bundle") {
				if let resourcesBundle = Bundle(path: resourcePath) {
					bundle = resourcesBundle
				}
			}
			
			var imageLeftArrow : UIImage!
			var imageRightArrow : UIImage!
			
			if #available(iOS 10.0, *) {
				imageLeftArrow = UIImage(named: "MKUIButtonBarArrowUp", in: bundle, compatibleWith: nil)
				imageRightArrow = UIImage(named: "MKUIButtonBarArrowDown", in: bundle, compatibleWith: nil)
			} else {
				imageLeftArrow = UIImage(named: "MKUIButtonBarArrowLeft", in: bundle, compatibleWith: nil)
				imageRightArrow = UIImage(named: "MKUIButtonBarArrowRight", in: bundle, compatibleWith: nil)
			}
			
						if #available(iOS 9.0, *) {
				imageLeftArrow = imageLeftArrow?.imageFlippedForRightToLeftLayoutDirection()
				imageRightArrow = imageRightArrow?.imageFlippedForRightToLeftLayoutDirection()
			}
			
			prev = MKUIBarButtonItem(image: imageLeftArrow, style: UIBarButtonItemStyle.plain, target: target, action: previousAction)
			prev.accessibilityLabel = "Toolbar Previous Button"
			
			next = MKUIBarButtonItem(image: imageRightArrow, style: UIBarButtonItemStyle.plain, target: target, action: nextAction)
			next.accessibilityLabel = "Toolbar Next Button"
			
						items.append(prev)
			
						let fixed = MKUIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
			if #available(iOS 10.0, *) {
				fixed.width = 6
			} else {
				fixed.width = 20
			}
			items.append(fixed)
			
						items.append(next)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let title = MKUITitleBarButtonItem(title: shouldHidePlaceholderText == true ? nil : titleText)
			items.append(title)
			
						items.append(UIView.flexibleBarButtonItem())
			
						let doneButton = MKUIBarButtonItem(title: rightButtonTitle, style: UIBarButtonItemStyle.done, target: target, action: rightButtonAction)
			items.append(doneButton)
			
						toolbar.items = items
			toolbar.toolbarTitleInvocation = self.titleInvocation
			
						if let textField = self as? UITextField {
				textField.inputAccessoryView = toolbar
				
				switch textField.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			} else if let textView = self as? UITextView {
				textView.inputAccessoryView = toolbar
				
				switch textView.keyboardAppearance {
				case UIKeyboardAppearance.dark:
					toolbar.barStyle = UIBarStyle.black
				default:
					toolbar.barStyle = UIBarStyle.default
				}
			}
		}
	}

	public func addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonTitle : String, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector, shouldShowPlaceholder : Bool) {
		
		var title : String?
		
		if shouldShowPlaceholder == true {
			title = self.drawingPlaceholderText
		}
		
		addPreviousNextRightOnKeyboardWithTarget(target, rightButtonTitle: rightButtonTitle, previousAction: previousAction, nextAction: nextAction, rightButtonAction: rightButtonAction, titleText: title)
	}

	public func setEnablePrevious ( _ isPreviousEnabled : Bool, isNextEnabled : Bool) {
		
				if let inputAccessoryView = self.inputAccessoryView as? MKUIToolbar {
						if inputAccessoryView.items?.count > 3 {
				if let items = inputAccessoryView.items {
					if let prevButton = items[0] as? MKUIBarButtonItem {
						if let nextButton = items[2] as? MKUIBarButtonItem {
							
							if prevButton.isEnabled != isPreviousEnabled {
								prevButton.isEnabled = isPreviousEnabled
							}
							
							if nextButton.isEnabled != isNextEnabled {
								nextButton.isEnabled = isNextEnabled
							}
						}
					}
				}
			}
		}
	}
}

