//
//  MKUIKeyboardReturnKeyHandler.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import UIKit

open class MKUIKeyboardReturnKeyHandler: NSObject, UITextFieldDelegate, UITextViewDelegate {
	open weak var delegate: (UITextFieldDelegate & UITextViewDelegate)?
	
	fileprivate var textFieldInfoCache = NSMutableSet()
	fileprivate let kMKUITextField = "kMKUITextField"
	fileprivate let kMKUITextFieldDelegate = "kMKUITextFieldDelegate"
	fileprivate let kMKUITextFieldReturnKeyType = "kMKUITextFieldReturnKeyType"
	
	open var lastTextFieldReturnKeyType: UIReturnKeyType = .default {
		
		didSet {
			for infoDict in textFieldInfoCache {
				if let view = (infoDict as AnyObject).object(forKey: kMKUITextField) as? UIView {
					updateReturnKeyTypeOnTextField(view)
				}
			}
		}
	}
	
	public override init() {
		super.init()
	}
	
	public init(controller: UIViewController) {
		super.init()
		addResponderFromView(controller.view)
	}
	
	deinit {
		
		for infoDict in textFieldInfoCache {
			
			let view = (infoDict as AnyObject).object(forKey: kMKUITextField) as? UIView
			
			if let textField = view as? UITextField {
				
				let returnKeyTypeValue = (infoDict as AnyObject).object(forKey: kMKUITextFieldReturnKeyType) as! NSNumber
				textField.returnKeyType = UIReturnKeyType(rawValue: returnKeyTypeValue.intValue)!
				
				textField.delegate = (infoDict as AnyObject).object(forKey: kMKUITextFieldDelegate) as? UITextFieldDelegate
			} else if let textView = view as? UITextView {
				
				textView.returnKeyType = UIReturnKeyType(rawValue: ((infoDict as AnyObject).object(forKey: kMKUITextFieldReturnKeyType) as! NSNumber).intValue)!
				
				let returnKeyTypeValue = (infoDict as AnyObject).object(forKey: kMKUITextFieldReturnKeyType) as! NSNumber
				textView.returnKeyType = UIReturnKeyType(rawValue: returnKeyTypeValue.intValue)!
				
				textView.delegate = (infoDict as AnyObject).object(forKey: kMKUITextFieldDelegate) as? UITextViewDelegate
			}
		}
		
		textFieldInfoCache.removeAllObjects()
	}
	
	fileprivate func textFieldViewCachedInfo(_ textField : UIView) -> [String : AnyObject]? {
		
		for infoDict in textFieldInfoCache {
			
			if (infoDict as AnyObject).object(forKey: kMKUITextField) as! NSObject == textField {
				return infoDict as? [String : AnyObject]
			}
		}
		
		return nil
	}
	
	fileprivate func updateReturnKeyTypeOnTextField(_ view: UIView) {
		var superConsideredView : UIView?
		
		for disabledClass in MKUIKeyboardManager.sharedManager().toolbarPreviousNextAllowedClasses {
			
			superConsideredView = view.superviewOfClassType(disabledClass)
			
			if superConsideredView != nil {
				break
			}
		}
		
		var textFields : [UIView]?
		
		if let unwrappedTableView = superConsideredView {
			textFields = unwrappedTableView.deepResponderViews()
		} else {
			
			textFields = view.responderSiblings()
			
			switch MKUIKeyboardManager.sharedManager().toolbarManageBehaviour {
			case .byTag:        textFields = textFields?.sortedArrayByTag()
			case .byPosition:   textFields = textFields?.sortedArrayByPosition()
			default:    break
			}
		}
		
		if let lastView = textFields?.last {
			
			if let textField = view as? UITextField {
				
				textField.returnKeyType = (view == lastView)    ?   lastTextFieldReturnKeyType : UIReturnKeyType.next
			} else if let textView = view as? UITextView {
				textView.returnKeyType = (view == lastView)    ?   lastTextFieldReturnKeyType : UIReturnKeyType.next
			}
		}
	}
	
	open func addTextFieldView(_ view: UIView) {
		var dictInfo: [String: AnyObject] = [String: AnyObject]()
		
		dictInfo[kMKUITextField] = view
		
		if let textField = view as? UITextField {
			dictInfo[kMKUITextFieldReturnKeyType] = textField.returnKeyType.rawValue as AnyObject?
			
			if let textFieldDelegate = textField.delegate {
				dictInfo[kMKUITextFieldDelegate] = textFieldDelegate
			}
			textField.delegate = self
		} else if let textView = view as? UITextView {
			dictInfo[kMKUITextFieldReturnKeyType] = textView.returnKeyType.rawValue as AnyObject?
			
			if let textViewDelegate = textView.delegate {
				dictInfo[kMKUITextFieldDelegate] = textViewDelegate
			}
			
			textView.delegate = self
		}
		
		textFieldInfoCache.add(dictInfo)
	}
	
	open func removeTextFieldView(_ view: UIView) {
		if let dict : [String : AnyObject] = textFieldViewCachedInfo(view) {
			
			if let textField = view as? UITextField {
				
				let returnKeyTypeValue = dict[kMKUITextFieldReturnKeyType] as! NSNumber
				textField.returnKeyType = UIReturnKeyType(rawValue: returnKeyTypeValue.intValue)!
				
				textField.delegate = dict[kMKUITextFieldDelegate] as? UITextFieldDelegate
			} else if let textView = view as? UITextView {
				
				let returnKeyTypeValue = dict[kMKUITextFieldReturnKeyType] as! NSNumber
				textView.returnKeyType = UIReturnKeyType(rawValue: returnKeyTypeValue.intValue)!
				
				textView.delegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
			
			textFieldInfoCache.remove(dict)
		}
	}
	
	open func addResponderFromView(_ view: UIView) {
		let textFields = view.deepResponderViews()
		
		for textField in textFields {
			addTextFieldView(textField)
		}
	}
	
	open func removeResponderFromView(_ view: UIView) {
		let textFields = view.deepResponderViews()
		
		for textField in textFields {
			removeTextFieldView(textField)
		}
	}
	
	@discardableResult fileprivate func goToNextResponderOrResign(_ view: UIView) -> Bool {
		var superConsideredView: UIView?
		
		for disabledClass in MKUIKeyboardManager.sharedManager().toolbarPreviousNextAllowedClasses {
			superConsideredView = view.superviewOfClassType(disabledClass)
			
			if superConsideredView != nil {
				break
			}
		}
		
		var textFields : [UIView]?
		
		if let unwrappedTableView = superConsideredView {
			textFields = unwrappedTableView.deepResponderViews()
		} else {
			
			textFields = view.responderSiblings()
			
			switch MKUIKeyboardManager.sharedManager().toolbarManageBehaviour {
			case .byTag:        textFields = textFields?.sortedArrayByTag()
			case .byPosition:   textFields = textFields?.sortedArrayByPosition()
			default:
				break
			}
		}
		
		if let unwrappedTextFields = textFields {
			if let index = unwrappedTextFields.index(of: view) {
				if index < (unwrappedTextFields.count - 1) {
					
					let nextTextField = unwrappedTextFields[index+1]
					nextTextField.becomeFirstResponder()
					return false;
				} else {
					
					view.resignFirstResponder()
					return true;
				}
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
	
	open func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		var aDelegate: UITextFieldDelegate? = delegate
		
		if (aDelegate == nil) {
			if let dict: [String: AnyObject] = textFieldViewCachedInfo(textField) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextFieldDelegate
			}
		}
		
		if let unDelegate = aDelegate {
			if unDelegate.responds(to: #selector(UITextFieldDelegate.textFieldShouldBeginEditing(_:))) {
				return unDelegate.textFieldShouldBeginEditing?(textField) == true
			}
		}
		
		return true
	}
	
	open func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
		var aDelegate : UITextFieldDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textField) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextFieldDelegate
			}
		}
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(UITextFieldDelegate.textFieldShouldEndEditing(_:))) {
				return unwrapDelegate.textFieldShouldEndEditing?(textField) == true
			}
		}
		
		return true
	}
	
	open func textFieldDidBeginEditing(_ textField: UITextField) {
		updateReturnKeyTypeOnTextField(textField)
		
		var aDelegate : UITextFieldDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textField) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextFieldDelegate
			}
		}
		
		aDelegate?.textFieldDidBeginEditing?(textField)
	}
	
	open func textFieldDidEndEditing(_ textField: UITextField) {
		
		var aDelegate : UITextFieldDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textField) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextFieldDelegate
			}
		}
		
		aDelegate?.textFieldDidEndEditing?(textField)
	}
	
	
	@available(iOS 10.0, *)
	open func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
		
		var aDelegate : UITextFieldDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textField) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextFieldDelegate
			}
		}
		
		aDelegate?.textFieldDidEndEditing?(textField, reason: reason)
	}
	
	open func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		var aDelegate : UITextFieldDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textField) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextFieldDelegate
			}
		}
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(UITextFieldDelegate.textField(_:shouldChangeCharactersIn:replacementString:))) {
				return unwrapDelegate.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) == true
			}
		}
		
		return true
	}
	
	open func textFieldShouldClear(_ textField: UITextField) -> Bool {
		
		var aDelegate : UITextFieldDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textField) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextFieldDelegate
			}
		}
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(UITextFieldDelegate.textFieldShouldClear(_:))) {
				return unwrapDelegate.textFieldShouldClear?(textField) == true
			}
		}
		
		return true
	}
	
	open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		var aDelegate : UITextFieldDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textField) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextFieldDelegate
			}
		}
		
		var shouldReturn = true;
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(UITextFieldDelegate.textFieldShouldReturn(_:))) {
				shouldReturn = unwrapDelegate.textFieldShouldReturn?(textField) == true
			}
		}
		
		if shouldReturn == true {
			goToNextResponderOrResign(textField)
			return true;
		} else {
			return goToNextResponderOrResign(textField)
		}
	}
	
	open func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(UITextViewDelegate.textViewShouldBeginEditing(_:))) {
				return unwrapDelegate.textViewShouldBeginEditing?(textView) == true
			}
		}
		
		return true
	}
	
	open func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(UITextViewDelegate.textViewShouldEndEditing(_:))) {
				return unwrapDelegate.textViewShouldEndEditing?(textView) == true
			}
		}
		
		return true
	}
	
	open func textViewDidBeginEditing(_ textView: UITextView) {
		updateReturnKeyTypeOnTextField(textView)
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		aDelegate?.textViewDidBeginEditing?(textView)
	}
	
	open func textViewDidEndEditing(_ textView: UITextView) {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		aDelegate?.textViewDidEndEditing?(textView)
	}
	
	open func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		var shouldReturn = true
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(UITextViewDelegate.textView(_:shouldChangeTextIn:replacementText:))) {
				shouldReturn = (unwrapDelegate.textView?(textView, shouldChangeTextIn: range, replacementText: text)) == true
			}
		}
		
		if shouldReturn == true && text == "\n" {
			shouldReturn = goToNextResponderOrResign(textView)
		}
		
		return shouldReturn
	}
	
	open func textViewDidChange(_ textView: UITextView) {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		aDelegate?.textViewDidChange?(textView)
	}
	
	open func textViewDidChangeSelection(_ textView: UITextView) {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(textView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		aDelegate?.textViewDidChangeSelection?(textView)
	}
	
	@available(iOS 10.0, *)
	open func textView(_ aTextView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(aTextView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(textView as (UITextView, URL, NSRange, UITextItemInteraction) -> Bool)) {
				return unwrapDelegate.textView?(aTextView, shouldInteractWith: URL, in: characterRange, interaction: interaction) == true
			}
		}
		
		return true
	}
	
	@available(iOS 10.0, *)
	open func textView(_ aTextView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(aTextView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(textView as (UITextView, NSTextAttachment, NSRange, UITextItemInteraction) -> Bool)) {
				return unwrapDelegate.textView?(aTextView, shouldInteractWith: textAttachment, in: characterRange, interaction: interaction) == true
			}
		}
		
		return true
	}
	
	@available(iOS, deprecated: 10.0)
	open func textView(_ aTextView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(aTextView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(textView as (UITextView, URL, NSRange) -> Bool)) {
				return unwrapDelegate.textView?(aTextView, shouldInteractWith: URL, in: characterRange) == true
			}
		}
		
		return true
	}
	
	@available(iOS, deprecated: 10.0)
	open func textView(_ aTextView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange) -> Bool {
		
		var aDelegate : UITextViewDelegate? = delegate;
		
		if aDelegate == nil {
			
			if let dict : [String : AnyObject] = textFieldViewCachedInfo(aTextView) {
				aDelegate = dict[kMKUITextFieldDelegate] as? UITextViewDelegate
			}
		}
		
		if let unwrapDelegate = aDelegate {
			if unwrapDelegate.responds(to: #selector(textView as (UITextView, NSTextAttachment, NSRange) -> Bool)) {
				return unwrapDelegate.textView?(aTextView, shouldInteractWith: textAttachment, in: characterRange) == true
			}
		}
		
		return true
	}
}
