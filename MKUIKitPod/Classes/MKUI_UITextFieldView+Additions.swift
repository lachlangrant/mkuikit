//
//  MKUI_UITextFieldView+Additions.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import UIKit

public let kMKUIUseDefaultKeyboardDistance = CGFloat.greatestFiniteMagnitude

private var kMKUIKeyboardDistanceFromTextField = "kMKUIKeyboardDistanceFromTextField"

public extension UIView {
	
	public var keyboardDistanceFromTextField: CGFloat {
		get {
			
			if let aValue = objc_getAssociatedObject(self, kMKUIKeyboardDistanceFromTextField) as? CGFloat {
				return aValue
			} else {
				return kMKUIUseDefaultKeyboardDistance
			}
		}
		set(newValue) {
			objc_setAssociatedObject(self, kMKUIKeyboardDistanceFromTextField, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
		}
	}
}

