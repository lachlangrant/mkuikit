//
//  MKUI_UIScrollView+Additions.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import Foundation
import UIKit

private var kMKUIShouldRestoreScrollViewContentOffset = "kMKUIShouldRestoreScrollViewContentOffset"

public extension UIScrollView {
	
	public var shouldRestoreScrollViewContentOffset: Bool {
		get {
			
			if let aValue = objc_getAssociatedObject(self, kMKUIShouldRestoreScrollViewContentOffset) as? Bool {
				return aValue
			} else {
				return false
			}
		}
		set(newValue) {
			objc_setAssociatedObject(self, kMKUIShouldRestoreScrollViewContentOffset, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
		}
	}
}
