//
//  MKUIToast.swift
//  MKKit
//
//  Created by Lachlan Grant on 19/12/16.
//  Copyright © 2016 Lachlan Grant. All rights reserved.
//

import Foundation
import SwiftyDrop

/// MKUIToast
public class MKUIToast {
    static public func showMessage(_ message: String, hapticFeedback: Bool = false) {
        Drop.down(message) {
            if hapticFeedback == true {
                MKUITapticEngine.impact.prepare(.medium)
                MKUITapticEngine.impact.feedback(.medium)
            }
        }
    }
    
    static public func showMessage(_ message: String, state: DropState, duration: Double, hapticFeedback: Bool = false) {
        Drop.down(message, state: state, duration: duration) {
            if hapticFeedback == true {
                switch state {
                case .error:
                    MKUITapticEngine.notification.prepare()
                    MKUITapticEngine.notification.feedback(.error)
                    
                case .warning:
                    MKUITapticEngine.notification.prepare()
                    MKUITapticEngine.notification.feedback(MKUITapticEngine.Notification.NotificationType.warning)
                    
                case .success:
                    MKUITapticEngine.notification.prepare()
                    MKUITapticEngine.notification.feedback(MKUITapticEngine.Notification.NotificationType.success)
                    
                default:
                    MKUITapticEngine.impact.prepare(.medium)
                    MKUITapticEngine.impact.feedback(.medium)
                }
            }
        }
    }

}
