//
//  MKUITextView.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import UIKit

open class MKUITextView: UITextView {
	
	
	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}
	
	override init(frame: CGRect, textContainer: NSTextContainer?) {
		super.init(frame: frame, textContainer: textContainer)
		commonInit()
	}
	
	open override func awakeFromNib() {
		super.awakeFromNib()
		commonInit()
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	
	private func commonInit() {
		NotificationCenter.default.addObserver(self, selector: #selector(self.refreshPlaceholder), name: NSNotification.Name.UITextViewTextDidChange, object: self)
	}
	
	
	fileprivate var placeholderLabel: UILabel?
	@IBInspectable open var placeholder: String? {
		
		get {
			return placeholderLabel?.text
		}
		
		set {
			if placeholderLabel == nil {
				placeholderLabel = UILabel()
				
				placeholderLabel?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
				placeholderLabel?.lineBreakMode = .byWordWrapping
				placeholderLabel?.numberOfLines = 0
				placeholderLabel?.font = self.font
				placeholderLabel?.backgroundColor = UIColor.clear
				placeholderLabel?.textColor = UIColor(white: 0.7, alpha: 1.0)
				placeholderLabel?.alpha = 0
				addSubview(placeholderLabel!)
				
				placeholderLabel?.text = newValue
				refreshPlaceholder()
			}
		}
	}
	
	open override func layoutSubviews() {
		super.layoutSubviews()
		
		if let label = placeholderLabel {
			label.sizeToFit()
			label.frame = CGRect(x: 8, y: 8, width: self.frame.width - 16, height: self.frame.height)
		}
	}
	
	@objc open func refreshPlaceholder() {
		if text.characters.count != 0 {
			placeholderLabel?.alpha = 0
		} else {
			placeholderLabel?.alpha = 1
		}
	}
	
	override open var text: String! {
		didSet {
			refreshPlaceholder()
		}
	}
	
	override open var font: UIFont? {
		didSet {
			if let unWrapFont = font {
				placeholderLabel?.font = unWrapFont
			} else {
				placeholderLabel?.font = UIFont.systemFont(ofSize: 12)
			}
		}
	}
	
	override open var delegate: UITextViewDelegate? {
		
		get {
			refreshPlaceholder()
			return super.delegate
		}
		
		set {
			super.delegate = newValue
		}
	}
}















