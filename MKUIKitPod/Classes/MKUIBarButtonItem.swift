//
//  MKUIBarButtonItem.swift
//  MKUIKit
//
//  Created by Lachlan Grant on 19/3/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import UIKit

open class MKUIBarButtonItem: UIBarButtonItem {
	
	public override init() {
		super.init()
		
		//Tint color
		self.tintColor = nil
		
		//Title
		self.setTitlePositionAdjustment(UIOffset.zero, for: UIBarMetrics.default)
		self.setTitleTextAttributes(nil, for: UIControlState())
		self.setTitleTextAttributes(nil, for: UIControlState.highlighted)
		self.setTitleTextAttributes(nil, for: UIControlState.disabled)
		self.setTitleTextAttributes(nil, for: UIControlState.selected)
		self.setTitleTextAttributes(nil, for: UIControlState.application)
		self.setTitleTextAttributes(nil, for: UIControlState.reserved)
		
		//Background Image
		self.setBackgroundImage(nil, for: UIControlState(),      barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.highlighted, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.disabled,    barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.selected,    barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.application, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.reserved,    barMetrics: UIBarMetrics.default)
		
		self.setBackgroundImage(nil, for: UIControlState(),      style: UIBarButtonItemStyle.done, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.highlighted, style: UIBarButtonItemStyle.done, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.disabled,    style: UIBarButtonItemStyle.done, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.selected,    style: UIBarButtonItemStyle.done, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.application, style: UIBarButtonItemStyle.done, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.reserved,    style: UIBarButtonItemStyle.done, barMetrics: UIBarMetrics.default)
		
		self.setBackgroundImage(nil, for: UIControlState(),      style: UIBarButtonItemStyle.plain, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.highlighted, style: UIBarButtonItemStyle.plain, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.disabled,    style: UIBarButtonItemStyle.plain, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.selected,    style: UIBarButtonItemStyle.plain, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.application, style: UIBarButtonItemStyle.plain, barMetrics: UIBarMetrics.default)
		self.setBackgroundImage(nil, for: UIControlState.reserved,    style: UIBarButtonItemStyle.plain, barMetrics: UIBarMetrics.default)
		self.setBackgroundVerticalPositionAdjustment(0, for: UIBarMetrics.default)
		
		//Back Button
		self.setBackButtonBackgroundImage(nil, for: UIControlState(),      barMetrics: UIBarMetrics.default)
		self.setBackButtonBackgroundImage(nil, for: UIControlState.highlighted, barMetrics: UIBarMetrics.default)
		self.setBackButtonBackgroundImage(nil, for: UIControlState.disabled,    barMetrics: UIBarMetrics.default)
		self.setBackButtonBackgroundImage(nil, for: UIControlState.selected,    barMetrics: UIBarMetrics.default)
		self.setBackButtonBackgroundImage(nil, for: UIControlState.application, barMetrics: UIBarMetrics.default)
		self.setBackButtonBackgroundImage(nil, for: UIControlState.reserved,    barMetrics: UIBarMetrics.default)
		
		self.setBackButtonTitlePositionAdjustment(UIOffset.zero, for: UIBarMetrics.default)
		self.setBackButtonBackgroundVerticalPositionAdjustment(0, for: UIBarMetrics.default)
	}
	
	required public init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
